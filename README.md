# Dina

Dina is a simple asynchronous, incomplete web server implementation, with no externall dependencies. It is not
production ready and has not been tested under any kind of load or stress test. It has support for HTTP 1.1 and
Keep Alive but misses support for chunked transfer encoding.

# Run

With Maven installed, please issue in the the following commands:

    mvn clean compile exec:java

or

    mvn clean compile jar:jar
    java -jar target/dina-0.1a.jar


If everything works alright an instance of the server will be started and accessible at the following location:
<http://localhost:8081>

The main class name of the web server is `org.pereni.dina.http.HttpServer`. At this point it takes no arguments and
expects a `www` directory to be found in the current folder, directory that will be exposed by the server.

The server implements a subset of the Java `Servlet`, `HttpServletRequest`, `HttpServletResponse` API two servlet
implementations:

 1. `FileServlet` that provides a simple directory listing as well as resource serving both for regular browsers or
  as an REST API in case the request "Accepts" only `application/json` content type
 2. `SnoopServlet` mapped on `/snoop/*` that will echo the most important details of the request

At <http://localhost:8081/dragdrop_main.html> there is a simple application that uses the FileServlet REST Api to
display a simple navigator and builds upon the HTML5 Drag&Drop API to showcase the upcoming API (it is not cross browser
being tested only on: Chrome 28 (OSX), Chrome 27 (Win 7), Safari 6 (OSX), Firefox 21 (OSX), Firefox 13 Win.

While dropping multiple elements is supported, dragging multiple elements trough the HTML5 Drag & Drop API is only
supported on Mozilla trough a custom, non standard API (event.dataTransfer.mozClearDataAt)..

# Architecture

The implementation it is based on a flavor of the Reactor-Acceptor pattern. A big picture of the class relationships
can be seen in the image bellow.

![Image](https://bitbucket.org/remuspereni/dina/raw/d7d36df34367a964d220f344358626f6cf907a5b/www/images/diagram.jpg?raw=true)