
/**
 * Module containing the JavaScript part for our D&D implementation
 */
var DinaDD = ( function(){

    /* Private Functions */

    /**
     * Make AJAX GET Request
     * @param url The request URL
     * @param callback A callback function invoked in case of a success on the XHR instance (this)
     */
    function ajaxGet ( url, callback ) {
        var req = new XMLHttpRequest();
        req.callback = callback;
        req.arguments = Array.prototype.slice.call(arguments, 2);
        req.onload = handleAjaxSuccess;
        req.onerror = handleAjaxError;
        req.open("GET", url, true);
        req.setRequestHeader("Accept","application/json");
        req.send(null);
    }

    /**
     * Make AJAX POST Request
     * @param url The request URL
     * @param callback A callback function invoked in case of a success on the XHR instance (this)
     * @param bodyStr The urlencoded name=value&.... of the POST body that will be sent to the backend
     */
    function ajaxPost ( url, callback, bodyStr ) {
        var req = new XMLHttpRequest();
        req.callback = callback;
        req.arguments = Array.prototype.slice.call(arguments, 2);
        req.onload = handleAjaxSuccess;
        req.onerror = handleAjaxError;
        req.open("POST", url, true);
        req.setRequestHeader("Accept","application/json");
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.send(bodyStr);
    }


    /**
     * On success AJAX handler
     */
    function handleAjaxSuccess () { this.callback.apply(this, this.arguments); }

    /**
     * On error AJAX handler, for now we just display the status to the console
     */
    function handleAjaxError () { console.error(this.statusText); }


    /**
     * Hanlder function for the dragstart event
     * @param event The event
     */
    function handleDragStart(event) {

        this.classList.add('dragged');

        event.dataTransfer.dropEffect = 'move';
        event.dataTransfer.setData("text/x-url", this.getAttribute("data-location"));

        parent.document.getElementById("overlay").style.display = 'block';
    }


    /**
     * Handler function for the dragover event
     * @param event The event
     */
    function handleDragOver (event) {
        // Necessary. Allows us to drop.
        if (event.preventDefault) {
            event.preventDefault();
        }
        return false;
    }


    /**
     * Handler function for the dragenter event
     * @param event The event
     */
    function handleDragEnter (event) {
        this.classList.add('over');
    }

    /**
     * Handler function for the dragleave event
     * @param event The event
     */
    function handleDragLeave(event) {
        this.classList.remove('over');
    }


    /**
     * Handler function for the drop event
     * @param event The event
     */
    function handleDrop (event) {
        // this / e.target is current target element.

        if (event.stopPropagation) {
            event.stopPropagation(); // stops the browser from redirecting.
        }

        var location = event.dataTransfer.getData("text/x-url");

        var toLocation = document.querySelector("header .location").innerHTML;

        ajaxPost(location, function() {
            var xhr = this;

            if( ( xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                alert(xhr.responseText );
            } else {
                alert( xhr.statusText);
            }
        }, "moveto=" + encodeURIComponent(toLocation));

        var overElements = document.querySelectorAll('.over');
        [].forEach.call( overElements, function (elem) {
            elem.classList.remove('over');
        });

        // See the section on the DataTransfer object.
        return false;
    }


    /**
     * Handler function for the dragend event
     * @param event The event
     */
    function handleDragEnd (event) {
        // this/e.target is the source node.

        var draggedElements = document.querySelectorAll('.dragged');
        [].forEach.call(draggedElements, function (elem) {
            elem.classList.remove('dragged');
        });

        parent.document.getElementById("overlay").style.display = 'none';
    }


    /**
     * Private method to render the file list returned by the backend
     * @param fileListArray Object with the details of the location interogated as returned by the file servlet
     */
    function renderFolderList( fileListArray ){

        var location = document.querySelector("header .location");
        location.innerHTML = fileListArray.path;

        var container = document.querySelector("section .filePanelContainer");
        container.innerHTML = '';

        var fragment = document.createDocumentFragment();
        var folderList = document.createElement('ul');
        fragment.appendChild( folderList);

        var content = '<ul>';
        var files = fileListArray.files;
        var len = files.length;
        for( var idx = 0; idx < len ; idx++ ){
            var item = document.createElement('li');
            item.innerHTML = files[idx].name;
            item.id = files[idx].name;
            item.className = 'itm';
            item.className += ' ' +  files[idx].type;
            item.title = "File size " + files[idx].size +". Click to open.";
            item.setAttribute('draggable', 'true');
            item.setAttribute('data-location', files[idx].href);
            item.addEventListener('dragstart', handleDragStart, false);
            item.addEventListener('click', (function(){
                var file = files[idx];
                if( file.type === "directory") {
                    return function() { DinaDD.loadFolder( file.href); }
                } else {
                    return function() { window.location.href = file.href; }
                }
            })(), false);
            folderList.appendChild(item);
        }

        container.appendChild(fragment);
    }


    var mod = {};

    /**
     * Public method to initialize the module / component. It will automatically load the ROOT context
     */
    mod.initialize = function() {
        this.loadFolder("/");

        filePanels = document.querySelectorAll('.filePanel');

        [].forEach.call(filePanels, function(filePanel) {
            filePanel.addEventListener('dragenter', handleDragEnter, false)
            filePanel.addEventListener('dragover', handleDragOver, false);
            filePanel.addEventListener('dragleave', handleDragLeave, false);
            filePanel.addEventListener('drop', handleDrop, false);
            filePanel.addEventListener('dragend', handleDragEnd, false);
        });
    }


    /**
     * Method to load the content of a speciffic location from the server
     * @param location The relative path of the location to be loaded
     */
    mod.loadFolder = function( location ) {
        ajaxGet( location, function() {
            var xhr = this;

            if( ( xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
                renderFolderList( JSON.parse( xhr.responseText ));
            }
        })
    }


    return mod;

})();


/* Initialize and start our handler when the document is ready for interaction */
window.addEventListener("load", function load(event){
    window.removeEventListener("load", load, false); //remove listener, no longer needed
    DinaDD.initialize();
},false);


