package org.pereni.dina.util;


import java.util.*;

/**
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class MultiMap {

    private HashMap<String, Object> store = new HashMap<String, Object>();


    /**
     *
     * @param key
     * @param value
     */
    public void put(String key, Object value){
        if( store.containsKey(key) ){
            if( store.get(key) instanceof List) {
                ((List)store.get(key)).add( value);
            } else {
                List list = new ArrayList();
                list.add( store.get(key));
                list.add( value );
                store.put(key, list);
            }
        } else {
            store.put(key, value);
        }
    }

    /**
     *
     * @param key
     * @param value
     */
    public void set(String key, Object value ) {
        store.put( key, value );
    }


    /**
     *
     * @param key
     * @return
     */
    public Object getValue(String key){
           if( !store.containsKey( key ) ) return null;

            Object value = store.get( key );
            if( value instanceof List ) return ( (List) value).get(0);
            else return value;
    }


    /**
     *
     * @param key
     * @return
     */
    public List getValues(String key){
        if( !store.containsKey( key )) return null;

        Object value = store.get( key );
        if( value instanceof List ) return (List) value;
        else {
            List result = new ArrayList();
            result.add(value);
            return result;
        }
    }


    /**
     *
     * @param key
     * @return
     */
    public String[] getValuesAsArray(String key){
        if( !store.containsKey( key )) return null;

        Object value = store.get( key );
        if( value instanceof List ){
            String[] result = new String[ ((List)value).size() ];
            for( int idx = 0; idx < ((List) value).size(); idx++ ){
                result[idx] = (String) ((List)value).get(idx);
            }
            return result;
        } else {
            return new String[]{ (String)value};
        }
    }


    /**
     *
     * @param key
     * @return
     */
    public Object getActualValue(String key){
        return store.get( key );
    }


    /**
     *
     * @return
     */
    public Set<String> getKeys() {
        return store.keySet();
    }


    /**
     *
     * @param key
     * @return
     */
    public boolean containsKey( String key ) {
            return store.containsKey( key );
    }


    /**
     *
     * @return
     */
    public int size() {
        return store.size();
    }

}
