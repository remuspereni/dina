package org.pereni.dina.server;

import java.nio.channels.SelectableChannel;

/**
 * Defines an interface for registering, removing, and dispatching Event Handlers. Ultimately, the Synchronous Event
 * De-multiplexer is responsible for waiting until new events occur. When it detects new events, it informs the
 * Acceptor to create the actual event handlers in case it is a new connection and call back application-specific event
 * handlers. Common events include connection acceptance events, data input and output events.
 * @author Remus Pereni <remus@pereni.org>
 */
public interface Dispatcher {

    /**
     * Starts listening on the specified port.
     * @param port The TCP-IP port on which the dispatcher will listen to.
     */
    public void start(int port);

    /**
     * Stops the dispatcher.
     */
    public void stop();

    /**
     * Checks if the dispatcher is still running
     * @return true if the dispatcher is running false otherwise
     */
    public boolean isRunning();

    /**
     * Registers a request handler on a specific NIO channel for the operations the request handler
     * indicates trough the isReadyForOutput and isReadyForClose methods.
     * @param channel The NIO channel on which the handler wants to be registered
     * @param requestHandler The request handler that will be registered
     */
    public void registerHandler(SelectableChannel channel, RequestHandler requestHandler);


    /**
     * Removes an event handler from the operations at the end of it's life cycle.
     * @param handler the event handler that reached end of life cycle.
     */
    public void deRegisterHandler(RequestHandler handler);

    /**
     * Informs the dispatcher that the handler state has changed and it probably involves a change in the events
     * it wants to be notified about.
     * @param handler The event handler that has a new state and thus probably a different set of events it is
     *                interested into.
     */
    public void stateChanged(RequestHandler handler);

}
