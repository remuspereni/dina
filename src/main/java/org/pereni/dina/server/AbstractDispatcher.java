package org.pereni.dina.server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.Map;
import java.util.Iterator;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides a abstract generic TCP-IP / Java Nio based network event based dispatcher implementation.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public abstract class AbstractDispatcher implements Dispatcher, Runnable, RequestHandlerFactory,  Acceptor {


    /**
     * Logger instance
     */
    private static final Logger LOG = Logger.getLogger( AbstractDispatcher.class.getName());

    /**
     * Period over which we check for validity every request handler
     */
    private static final int PERIODIC_EXECUTOR_TIME = 5;

    /**
     * The Java NIO selector instance that we use
     */
    private Selector nioSelector;

    /**
     * The server socket channel that we listen to for new connections
     */
    private ServerSocketChannel serverSocketChannel;

    /**
     * The main thread that we are using to run the current dispatcher
     */
    private Thread thread;

    /**
     * Start a thread pool for running the request handlers. The number of thread available to the thread pool
     * will be equal with the number of logical cores available to the JVM
     */
    private final ExecutorService executor = Executors.newFixedThreadPool( Runtime.getRuntime().availableProcessors());

    /**
     * Map holding the association between the request handler and the channel. We are using it also for running the
     * periodic checker to check for stale or old handlers that did not get a chance to exit and clean up properly.
     */
    private final Map<RequestHandler, SelectableChannel> register
            =new ConcurrentHashMap<RequestHandler, SelectableChannel>();


    /**
     * The main constructor.
     * @throws IOException  If any exceptions happen during server socket channel open or selector.open
     */
    public AbstractDispatcher() throws IOException {
        this.serverSocketChannel = ServerSocketChannel.open();
        this.nioSelector = Selector.open();
    }


    /**
     * Starts listening on the specified port.
     * @param port The TCP-IP port on which the dispatcher will listen to.
     */
    public void start(int port) {
        try {
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.register( nioSelector, SelectionKey.OP_ACCEPT);
            serverSocketChannel.socket().bind( new InetSocketAddress(port));


            ScheduledExecutorService periodicChecker = Executors.newSingleThreadScheduledExecutor();
            periodicChecker.scheduleAtFixedRate( new Runnable(){

                    @Override
                    public void run() {
                        LOG.finer("Periodic executor running over " + register.size() + " handlers to check validity");

                        try {
                            for( RequestHandler handler : register.keySet() ) {
                                LOG.finest("Handler found registered with dispatcher " + handler.toString());

                                if( handler.isInValid() ) {
                                    handler.destroy();
                                }
                            }
                        } catch (Exception ex ){
                            ex.printStackTrace();
                            LOG.log(Level.WARNING, "Exception while running the periodic executor to clean up: ", ex);
                        }
                    }
                }
                    , PERIODIC_EXECUTOR_TIME, PERIODIC_EXECUTOR_TIME, TimeUnit.SECONDS);

        } catch (ClosedChannelException e) {
            LOG.log(Level.SEVERE, "Unable to start dispatcher on port {0}, reason: {1}"
                    , new String[]{ "" + port, e.getMessage()});
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Unable to start dispatcher on port {0}, reason: {1}"
                    , new String[]{"" + port, e.getMessage()});
        }

        thread = new Thread(this);
        thread.start();
    }


    /**
     * Stops the dispatcher.
     */
    @Override
    public void stop() {
        try {
            thread.interrupt();
        } catch(Exception ex) {
            LOG.log(Level.WARNING, ex.getMessage(), ex);
        }
        nioSelector.wakeup();
    }


    /**
     * Checks if the dispatcher is still running
     * @return true if the dispatcher is running false otherwise
     */
    public boolean isRunning() {
        return thread != null && thread.isAlive();
    }


    /**
     * Informs the dispatcher that the handler state has changed and it probably involves a change in the events
     * it wants to be notified about.
     * @param handler The event handler that has a new state and thus probably a different set of events it is
     *                interested into.
     */
    public void stateChanged(RequestHandler handler) {
        SelectableChannel channel = register.get( handler);

        if( channel == null ) {
            LOG.warning("Unable to find channel in register for handler " + handler);
            return;
        }


        if( handler.isReadyForClose() )  {

            deRegisterHandler( channel );

            if( channel.isOpen() ) try {
                channel.close();
            } catch (IOException e) {
                LOG.log( Level.WARNING, "Exception while closing the channel ", e);
            }

        } else {

            int operationsOfInterest = SelectionKey.OP_READ;
            if( handler.isReadyForOutput() ) operationsOfInterest = operationsOfInterest | SelectionKey.OP_WRITE;

            if( LOG.isLoggable( Level.FINE)) {
                LOG.log(Level.FINE, "Register handler received for channel {0}, operations {1}, handler {2}"
                    , new String[]{channel.toString(), "" + operationsOfInterest, handler.toString()});
            }

            try {
                channel.register(nioSelector, operationsOfInterest, handler);
            } catch (ClosedChannelException e) {
                LOG.log( Level.WARNING, "Unable to register channel, channel already closed", e);
            }

            nioSelector.wakeup();
        }
    }


    /**
     * Registers a request handler on a specific NIO channel for the operations the request handler
     * indicates trough the isReadyForOutput and isReadyForClose methods.
     * @param channel The NIO channel on which the handler wants to be registered
     * @param requestHandler The request handler that will be registered
     */
    public void registerHandler(SelectableChannel channel, RequestHandler requestHandler) {
        if( !channel.isOpen() ) {
            LOG.log( Level.WARNING, "Register handler received but channel is closed");
        }

        if( !register.containsKey( requestHandler) ) {
            register.put(requestHandler, channel);
        }
        stateChanged(requestHandler);
    }


    /**
     * Removes an event handler from the operations at the end of it's life cycle.
     * @param handler the event handler that reached end of life cycle.
     */
    public void deRegisterHandler(RequestHandler handler) {
        if( LOG.isLoggable( Level.FINE) ) {
            LOG.fine("De-register handler received for handler " + handler);
        }

        SelectableChannel channel = register.get( handler);
        if( channel != null ) {
            register.remove( handler);
            deRegisterHandler(channel);
        }
    }


    /**
     * Do the actual cancelling of the selection key and make sure the attached value to the key is nullified.
     * @param channel
     */
    protected void deRegisterHandler(SelectableChannel channel) {
        SelectionKey key = channel.keyFor(nioSelector);

        if( key != null ) {
            key.attach(null);
            key.cancel();
            nioSelector.wakeup();
        }
    }

    /**
     * Run the dispatcher in a thread. The thread will check to see if there are new events on the network socket or
     * on the Input or Output channels and invoke the right request handlers with the right operations in order
     * to have the proper operations.
     */
    @Override
    public void run() {
        while( !Thread.interrupted() ) {
            try {

                LOG.fine("Waiting for the next selector ");
                int channelsChanged = nioSelector.select();

                LOG.fine("selector events " + channelsChanged);

                Iterator<SelectionKey> keyIterator  = nioSelector.selectedKeys().iterator();

                while( keyIterator.hasNext() ) {
                    SelectionKey selectionKey = keyIterator.next();

                    if( LOG.isLoggable( Level.FINE)) {
                        LOG.fine("Channels selected " + channelsChanged
                            + " isRead " + selectionKey.isReadable()
                            + " isWrite " + selectionKey.isWritable()
                            + " isAcceptable " + selectionKey.isAcceptable()
                        );
                    }

                    if( selectionKey.isAcceptable() ) {
                        acceptConnection(serverSocketChannel, nioSelector, this);
                        keyIterator.remove();
                        continue;
                    }

                    RequestHandler requestHandler = (RequestHandler) selectionKey.attachment();

                    if( requestHandler.isWorking() ) {
                        if( LOG.isLoggable( Level.FINE)) {
                            LOG.fine("Handler is working, skipping " + requestHandler.toString());
                        }
                        continue;
                    }

                    keyIterator.remove();

                    if( selectionKey.isReadable() ) {
                        requestHandler.setInputAvailable((ReadableByteChannel) selectionKey.channel());
                        executor.submit(requestHandler);
                    }

                    if( selectionKey.isWritable() ) {
                        requestHandler.setOutputAvailable((WritableByteChannel) selectionKey.channel());
                        executor.submit(requestHandler);
                    }
                }
            } catch (CancelledKeyException ex) {
                LOG.fine("selectionKey was canceled in operation");
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }


    /**
     * Method that establishes a connection and initializes the event/request handler able to handle the connection
     * and protocol.
     *
     * @param serverSocketChannel the server socket channel this server employs to listen for connections. The
     *                            server socket is ready to accept a new connection at the time of call of this method.
     * @param selector the Java NIO selector the signalized that a new connection is ready to be established
     * @param dispatcher the dispatcher responsible for handling the event registration and coordination
     *
     * @return the particular instance of a request handler able to handle the communication with the client on the
     * particular instance of network protocol chosen
     *
     * @throws IOException in case a network or socket exception happen during the connection phase
     */
    @Override
    public RequestHandler acceptConnection( ServerSocketChannel serverSocketChannel
            , Selector selector
            , Dispatcher dispatcher) throws IOException {
        SocketChannel channel = serverSocketChannel.accept();
        channel.configureBlocking(false);

        RequestHandler requestHandler = createRequestHandler( channel.socket() );

        if( LOG.isLoggable( Level.FINE)) {
            LOG.fine("Accepting new connection, new request handler created " + requestHandler);
        }

        registerHandler(channel, requestHandler);
        return requestHandler;
    }

}
