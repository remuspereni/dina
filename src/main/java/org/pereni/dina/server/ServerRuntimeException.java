package org.pereni.dina.server;

/**
 * Custom error message that can pass a message code and description to a handler. Example status code and
 * status message in case of an Http server.
 * @author Remus Pereni <remus@pereni.org>
 */
public class ServerRuntimeException extends RuntimeException {

    protected int exceptionCode = -1;

    /**
     * Default constructor
     */
    public ServerRuntimeException() {
        super();
    }

    /**
     *
     * @param message
     */
    public ServerRuntimeException(String message) {
        super(message);
    }


    /**
     *
     * @param exceptionCode
     * @param message
     */
    public ServerRuntimeException(int exceptionCode, String message) {
        super(message);
        this.exceptionCode = exceptionCode;
    }


    /**
     *
     * @param message
     * @param cause
     */
    public ServerRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }


    /**
     *
     * @param exceptionCode
     * @param message
     * @param cause
     */
    public ServerRuntimeException(int exceptionCode, String message, Throwable cause) {
        super(message, cause);
        this.exceptionCode = exceptionCode;
    }


    /**
     *
     * @return
     */
    public int getExceptionCode() {
        return exceptionCode;
    }
}
