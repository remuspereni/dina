package org.pereni.dina.server;

import java.net.Socket;

/**
 * Provides a method by which a particular instance or type of a Handler can be created based on a open or new socket
 * connection.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public interface RequestHandlerFactory {

    /**
     * Creates a new instance of an request handler based on the accepted network socket instance.
     * @param socket the socket of a new client connection
     * @return the particular instance of a request handler able to handle the communication with the client on the
     * particular instance of network protocol chosen
     */
    public RequestHandler createRequestHandler(Socket socket);
}
