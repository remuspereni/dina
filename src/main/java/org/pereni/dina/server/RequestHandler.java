package org.pereni.dina.server;

import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

/**
 * Specifies an interface consisting of hook method that abstractly represents the dispatching operation for
 * service-speciﬁc events. This method must be implemented by application-speciﬁc services. 
 * @author Remus Pereni <remus@pereni.org>
 */
public interface RequestHandler extends Runnable {

    /**
     * Returns whether there is an input event available that was not yet processed by the handler
     * @return true if an input events is available to be processed
     */
    public boolean isInputAvailable();

    /**
     * Informs the handler that there is a input event available
     * @param inputChannel The channel that has input operations available for the handler
     */
    public void setInputAvailable(ReadableByteChannel inputChannel);

    /**
     * Generic hook method for handling the input event
     */
    public void handleInput();

    /**
     * Generic hook for handling the output event.
     */
    public void handleOutput();

    /**
     * Returns true whether is an output event available that was not yet processed by the handler
     * @return true if the request handler can perform an output event.
     */
    public boolean isOutputAvailable();

    /**
     * Informs the handler that the communication channel is ready for an output event
     * @param outputChannel The output channel on which the output can be performed
     */
    public void setOutputAvailable(WritableByteChannel outputChannel);

    /**
     * Method that allows the handler to inform others that it reached end of lifecycle and resources can be closed
     * @return true if the handle finished serving it's request
     */
    public boolean isReadyForClose();

    /**
     * Method that allows the handler to inform the dispatcher that is ready for output
     * @return true if the handler is ready for output, false otherwise
     */
    public boolean isReadyForOutput();

    /**
     * Method that allows the dispatcher to be informed if the handler is in the middle of an operation
     * @return true if the handler is in the middle of an operation, false if it is waiting
     */
    public boolean isWorking();

    /**
     * Generic hook that allows a handler to inform the dispatcher that it is no longer valid and can be destroyed
     * @return true if the handler is at the end of it's lifecycle, false otherwise
     */
    public boolean isInValid();

    /**
     * Destroy the handler, release all the resources that are used by it.
     */
    public void destroy();

}
