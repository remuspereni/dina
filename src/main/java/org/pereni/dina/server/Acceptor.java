package org.pereni.dina.server;

import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

/**
 * Interface that allows a factory to be created that provides a strategy to establish a connection and initialize
 * the associated event / request handler.
 *
 * @see org.pereni.dina.server.RequestHandlerFactory
 * @see org.pereni.dina.server.RequestHandler
 * @author Remus Pereni <remus@pereni.org>
 */
public interface Acceptor {


    /**
     * Method that establishes a connection and initializes the event/request handler able to handle the connection
     * and protocol.
     *
     * @param serverSocketChannel the server socket channel this server employs to listen for connections. The
     *                            server socket is ready to accept a new connection at the time of call of this method.
     * @param selector the Java NIO selector the signalized that a new connection is ready to be established
     * @param dispatcher the dispatcher responsible for handling the event registration and coordination
     *
     * @return the particular instance of a request handler able to handle the communication with the client on the
     * particular instance of network protocol chosen
     *
     * @throws IOException in case a network or socket exception happen during the connection phase
     */
    public RequestHandler acceptConnection(ServerSocketChannel serverSocketChannel
            , Selector selector
            , Dispatcher dispatcher) throws IOException;
}
