package org.pereni.dina.server;

import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides an abstract implementation of the generic operations of an event / request handler. Actual service
 * implementations need to provide the handleInput and handleOutput methods. It is meant to be run in a thread so
 * it does not block the dispatcher.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public abstract class AbstractRequestHandler implements RequestHandler {

    /**
     * Class logger.
     */
    private static final Logger LOG = Logger.getLogger(AbstractRequestHandler.class.getName());


    /**
     * Internal flag used by the request handler to signal that is ready for outputting content.
     */
    private boolean isReadyForOutput = false;


    /**
     * Internal flag used by the request handler to signal that it reached it's end of life cycle and can be closed
     * and all the associated resources requested.
     */
    private boolean isReadyForClose = false;


    /**
     * Internal flag usually set by the dispatcher to signal that input content is available for processing
     */
    private boolean isInputAvailable = false;


    /**
     * Internal flag usually set by the dispatcher to signal that the output is available and the handler can
     * proceed at sending the desired content
     */
    private boolean isOutputAvailable = false;


    /**
     * Internal flag signaling that the handler is currently in an operation and therefore until it finished it can
     * not handle new events.
     */
    protected AtomicBoolean isWorking = new AtomicBoolean(false);


    /**
     * The input channel as supplied by the dispatcher in case there was an input data available event from the client
     */
    private ReadableByteChannel inputChannel;


    /**
     * The output channel as supplied by the dispatcher in case there was an output data available event from the client
     */
    private WritableByteChannel outputChannel;


    /**
     * An instance of the dispatcher handling this request handler, used for de-registering the handler or notifying
     * the dispatcher in case of an state change (ready for output, ready for close).
     */
    private Dispatcher dispatcher;


    /**
     * Default constructor of an event handler.
     * @param dispatcher the instance of the dispatcher handling this request handler, used for de-registering the
     *                   handler or notifying the dispatcher in case of an state change (ready for output,
     *                   ready for close)
     */
    protected AbstractRequestHandler(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }


    /**
     * Returns whether there is an input event available that was not yet processed by the handler
     * @return true if an input events is available to be processed
     */
    @Override
    public boolean isInputAvailable() {
        return isInputAvailable;
    }


    /**
     * Informs the handler that there is a input event available
     * @param inputChannel The channel that has input operations available for the handler
     */
    @Override
    public void setInputAvailable(ReadableByteChannel inputChannel) {
        if( inputChannel != null ) {
            this.isInputAvailable = true;
            this.inputChannel = inputChannel;
        } else {
            this.isInputAvailable = false;
            this.inputChannel = null;
        }

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Input available " + toString());
        }
    }


    /**
     * Method that allows the handler to inform the dispatcher that is ready for output
     * @return true if the handler is ready for output, false otherwise
     */
    public boolean isReadyForOutput() {
        return isReadyForOutput;
    }


    /**
     * Marks this handler ready to perform output. It's dispatcher will be called with a state changed notification
     * moment in which the dispatcher will register this handler to be notified when the output channel actually
     * gets available.
     *
     * @param readyForOutput true when the handler is ready to send output, false otherwise.
     */
    protected void setReadyForOutput(boolean readyForOutput) {
        this.isReadyForOutput = readyForOutput;

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Ready for output " + toString());
        }
        dispatcher.stateChanged(this);
    }


    /**
     * Returns true whether is an output event available that was not yet processed by the handler
     * @return true if the request handler can perform an output event.
     */
    @Override
    public boolean isOutputAvailable() {
        return isOutputAvailable;
    }


    /**
     * Informs the handler that the communication channel is ready for an output event
     * @param outputChannel The output channel on which the output can be performed
     */
    @Override
    public void setOutputAvailable(WritableByteChannel outputChannel) {
        if( outputChannel != null  ) {
            this.outputChannel = outputChannel;
            this.isOutputAvailable = true;
        } else {
            this.outputChannel = null;
            this.isOutputAvailable = false;
        }

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Output available " + toString());
        }
    }


    /**
     * The runnable implementation that will check if there is input available that was not yet processed case in
     * which the handleInput method will be invoked, followed by a check if output is available case in which
     * the handleOutput method will be invoked.
     */
    @Override
    public void run() {
        isWorking.set(true);

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Starting handler " + toString());
        }


        if( isInputAvailable() ) {
            handleInput();
            isInputAvailable = false;
        }

        if( isOutputAvailable() ) {
            handleOutput();
            isOutputAvailable = false;
        }

        if( isReadyForClose() ) {
            dispatcher.deRegisterHandler(this);
        }

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Stopping handler " + toString());
        }

        isWorking.set(false);
    }


    /**
     * Method that allows the handler to inform others that it reached end of lifecycle and resources can be closed
     * @return true if the handle finished serving it's request
     */
    @Override
    public boolean isReadyForClose() {
        return isReadyForClose;
    }


    /**
     * Marks this handler ready to be destroyed. It's dispatcher will be called with a state changed notification
     * moment in which the dispatcher will will cancel it's event notifications and destroy any resources
     * associated with this handler.
     *
     * @param readyForClose true when the handler is ready to close, false otherwise.
     */
    protected void setReadyForClose(boolean readyForClose) {
        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Ready for close "  + readyForClose + " handler "  + toString());
        }

        this.isReadyForClose = readyForClose;
        dispatcher.stateChanged(this);
    }


    /**
     * Method that allows the dispatcher to be informed if the handler is in the middle of an operation
     * @return true if the handler is in the middle of an operation, false if it is waiting
     */
    @Override
    public boolean isWorking() {
        return isWorking.get();
    }


    /**
     * Getter for the input channel
     * @return the input channel if available (was set trough setInputAvailable) or false in case there was no
     * input event yet.
     */
    public ReadableByteChannel getInputChannel() {
        return inputChannel;
    }


    /**
     * Getter for the output channel
     * @return the output channel if available (was set trough setInputAvailable) or false in case there was no
     * output event yet.
     */
    public WritableByteChannel getOutputChannel() {
        return outputChannel;
    }


    /**
     * Override for the toString method to return some debugging information about the current workers state.
     *
     * @return some debugging information about the current workers state
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("{ id:'").append(hashCode()).append("'");
        result.append(", thread:").append( Thread.currentThread().getName());
        result.append(", isWrk:").append(isWorking.get());
        result.append(", isInAv:").append(isInputAvailable);
        result.append(", isOutAv:").append(isOutputAvailable);
        result.append(", isROut:").append(isReadyForOutput);
        result.append(", isRCls:").append(isReadyForClose);
        result.append("}");
        return result.toString();
    }


    /**
     * Generic hook that allows a handler to inform the dispatcher that it is no longer valid and can be destroyed
     * @return true if the handler is at the end of it's lifecycle, false otherwise
     */
    @Override
    public boolean isInValid() {
        return true;
    }
}
