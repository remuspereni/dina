package org.pereni.dina.server;


/**
 * A a generic server implementation. The actual functionality depends
 * on the type of dispatcher supplied. This seems as a useless indirection towards dispatcher but in normal
 * circumstances here we would take various command line parameters and would perform various configuration
 * and initialization operations.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class ServerAdapter implements Server {

    /**
     * The default port
     */
    private static final int DEFAULT_PORT = 8081;

    /**
     * Current port used by the server
     */
    private int port = DEFAULT_PORT;

    /**
     * The actual dispatcher that the server will start
     */
    private Dispatcher dispatcher;


    /**
     * Creates an instance of a server with the supplied dispatcher as responsible of the actual
     * serving functionality.
     * @param dispatcher The dispatcher performing all the activities of the server
     */
    public ServerAdapter(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    /**
     * Default constructor. This require a dispatcher to be able to function as such it can not
     * be started until an actual dispatcher is set.
     */
    public ServerAdapter() {

    }


    /**
     * Sets the dispatcher in charge of the actual activity of the server.
     * @param dispatcher the dispatcher in charge of the actual activity of the server.
     */
    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }


    /**
     * Returns the dispatcher at the core of this server.
     * @return the dispatcher in charge of the actual activity of the server.
     */
    public Dispatcher getDispatcher() {
        return dispatcher;
    }


    /**
     * Returns the value of the TCP-IP port on which the server listens
     * @return the value of the TCP-IP port on which the server listens
     */
    @Override
    public int getPort() {
        return port;
    }

    /**
     * Sets the port on which the server will be listening when started
     * @param port  the value of the TCP-IP port on which the server listens
     */
    @Override
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Starts the server on all interfaces and on the port specified or on
     * port 8081 by default
     */
    @Override
    public void start(){
        if( dispatcher == null ) throw new IllegalStateException("A dispatcher must be set for every server");

        dispatcher.start(getPort());
    }


    /**
     * Stops the server
     */
    @Override
    public void stop() {
        if( dispatcher == null ) throw new IllegalStateException("A dispatcher must be set for every server");

        dispatcher.stop();
    }

    /**
     * Verifies if the server is running
     * @return true if the server is running false otherwise
     */
    @Override
    public boolean isRunning() {
        if( dispatcher == null ) throw new IllegalStateException("A dispatcher must be set for every server");

        return dispatcher.isRunning();
    }
}
