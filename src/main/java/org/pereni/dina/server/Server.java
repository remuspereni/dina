package org.pereni.dina.server;

/**
 * Interface to a generic server.
 * @author Remus Pereni <remus@pereni.org>
 */
public interface Server {

    /**
     * Returns the value of the TCP-IP port on which the server listens
     * @return the value of the TCP-IP port on which the server listens
     */
    int getPort();

    /**
     * Sets the port on which the server will be listening when started
     * @param port  the value of the TCP-IP port on which the server listens
     */
    void setPort(int port);

    /**
     * Starts the server on all interfaces and on the port specified or on
     * port 8081 by default
     */
    void start();

    /**
     * Stops the server
     */
    void stop();

    /**
     * Verifies if the server is running
     * @return true if the server is running false otherwise
     */
    boolean isRunning();
}
