package org.pereni.dina.http;

import org.pereni.dina.server.AbstractDispatcher;
import org.pereni.dina.server.RequestHandler;
import org.pereni.dina.server.RequestHandlerFactory;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Provides a <code>Dispatcher</code> implementation able to handle Http protocol based requests. It is started by
 * default on port 8081 on all available interfaces.
 *
 * <p>It also provides a implementation of RequestHandlerFactory returning an instance of <code>HttpRequestHandler</code>
 * for each new <code>Socket</code> open by a client.</p>
 *
 * @author Remus Pereni <remus@pereni.org>
 *
 * @see org.pereni.dina.http.HttpRequestHandler
 */
public class HttpDispatcher extends AbstractDispatcher implements RequestHandlerFactory {

    /**
     * Class logger instance
     */
    private static final Logger LOG = Logger.getLogger( HttpDispatcher.class.getName());


    /**
     * The servlet context mapped on this dispatcher. At this implementation it can be only one.
     */
    private ServletContext servletContext;


    /**
     * The default constructor of the HttpDispatcher instance.
     * @param servletContext the servlet context that will be used in this dispatcher
     * @throws IOException in case there is a problem in establishing the server network socket.
     */
    public HttpDispatcher( ServletContext servletContext ) throws IOException {
        super();
        this.servletContext = servletContext;
    }


    /**
     * Provides an instance of <code>HttpRequestHandler</code> for each new <code>Socket</code> open by a client.
     * @param socket the socket of a new client connection
     * @return the new instance of a HttpRequestHandler
     * @see org.pereni.dina.http.HttpRequestHandler
     */
    @Override
    public RequestHandler createRequestHandler( Socket socket) {
        return new HttpRequestHandler(this, socket);
    }


    /**
     * Provides the servlet context mapped on this dispatcher.
     * @return the servlet context instance mapped on this dispatcher
     */
    public ServletContext getServletContext() {
        return servletContext;
    }

    /**
     * Sets the servlet context mapped on this dispatcher.
     * @param servletContext the servlet context instance mapped on this dispatcher
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
}
