package org.pereni.dina.http;

import org.pereni.dina.server.ServerRuntimeException;
import org.pereni.dina.util.MultiMap;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The actual implementation of the HttpRequest.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class HttpRequestImpl implements HttpRequest {

    /**
     * Class logger
     */
    private static final Logger LOG = Logger.getLogger( HttpRequestImpl.class.getName());

    /**
     * The request method
     */
    private Http.Method method = Http.Method.GET;

    /**
     * The request URI
     */
    private URI requestURI;

    /**
     * Http request version
     */
    private Http.Version version;


    /**
     * Http request headers
     */
    private MultiMap headers = new MultiMap();

    /**
     * Http request parameters
     */
    private MultiMap parameters = new MultiMap();

    /**
     * The request servlet context
     */
    private ServletContext servletContext;

    /**
     * The request remote ip address
     */
    private String remoteAddr = "";

    /**
     * The request remote host name
     */
    private String remoteHost = "";

    /**
     * The request remote port
     */
    private int remotePort = -1;

    /**
     * The local socket ip address
     */
    private String localAddr = "";

    /**
     * The local socket host name
     */
    private String localHost = "";

    /**
     * The local socket port number
     */
    private int localPort = -1;

    /**
     * The request character encoding
     */
    private String characterEncoding = "utf-8";


    /**
     * The request content type
     */
    private String contentType;


    /**
     *
     * @param requestURI
     */
    public void setRequestURI(URI requestURI){
        if( requestURI == null || !requestURI.getPath().startsWith("/")) {
            throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
        }
        processQueryParameters( requestURI.getQuery());
        this.requestURI = requestURI;
    }


    /**
     *
     * @param queryParameters
     */
    public void processQueryParameters(String queryParameters) {
        LOG.finer("Processing query parameter " + queryParameters);
        if( queryParameters == null ) return;

        StringTokenizer paramPair = new StringTokenizer (queryParameters, "&");

        while ( paramPair.hasMoreTokens ()) {
            String param = paramPair.nextToken ();

            String[] paramArr = param.split("=");
            try {
                String name = URLDecoder.decode( paramArr[0], getCharacterEncoding());
            String value = "";
            if( paramArr.length > 1 ) {
                value = URLDecoder.decode ( paramArr[1], getCharacterEncoding());
            }
            parameters.put(name, value);
            } catch (UnsupportedEncodingException e) {
                LOG.log( Level.WARNING, "Exception while decoding query parameters.", e);
            }
        }
    }


    /**
     *
     * @param name the header name
     * @return
     */
    @Override
    public String getHeader(String name) {
        if( headers.containsKey(name.toLowerCase()) ) return (String) headers.getValue(name.toLowerCase());
        return null;
    }


    /**
     *
     * @param name the header name
     * @return
     */
    @Override
    public Enumeration getHeaders(String name) {
        if( headers.containsKey( name.toLowerCase() ) ) {
            return Collections.enumeration(headers.getValues(name.toLowerCase()));
        }
        return Collections.enumeration( Collections.emptySet());
    }


    /**
     *
     * @return
     */
    @Override
    public Enumeration getHeaderNames() {
        return Collections.enumeration(headers.getKeys());
    }


    /**
     *
     * @param name
     * @return
     */
    @Override
    public int getIntHeader(String name) {
        if( headers.containsKey(name.toLowerCase()) ) {
            return Integer.parseInt( (String) headers.getValue(name.toLowerCase()));
        }
        return -1;
    }


    /**
     *
     * @return
     */
    @Override
    public String getMethod() {
        return method.name();
    }


    /**
     *
     * @param method
     */
    public void setMethod(Http.Method method) {
        if( method == null ) {
            throw new ServerRuntimeException( HttpResponse.SC_METHOD_NOT_ALLOWED, "Method Not Allowed");
        }
        this.method = method;
    }


    /**
     *
     * @return
     */
    @Override
    public String getPathInfo() {
        return requestURI.getPath();
    }


    /**
     *
     * @return
     */
    @Override
    public String getPathTranslated() {
        return getServletContext().getRealPath( getPathInfo());
    }


    /**
     *
     * @return
     */
    @Override
    public String getQueryString() {
        if( requestURI == null ) return null;
        return requestURI.getQuery();
    }


    /**
     *
     * @return
     */
    @Override
    public String getRequestURI() {
        if( requestURI == null ) return null;
        return requestURI.toString();
    }


    /**
     *
     * @return
     */
    @Override
    public StringBuffer getRequestURL() {
        try {
            return new StringBuffer( new URL( getProtocol(), getServerName(), getServerPort(), getRequestURI() ).toString());
        } catch (MalformedURLException e) {
            LOG.log(Level.SEVERE, "Unable to recompose URL " + toString(), e);
        }
        return null;
    }


    /**
     *
     * @return
     */
    @Override
    public String getCharacterEncoding() {
        return characterEncoding;
    }


    /**
     *
     * @param characterEncoding
     * @throws UnsupportedEncodingException
     */
    @Override
    public void setCharacterEncoding(String characterEncoding) throws UnsupportedEncodingException {
        // try to see if the supplied value is ok
        new String( new byte[]{}, characterEncoding);
        this.characterEncoding = characterEncoding;
    }


    /**
     *
     * @return
     */
    @Override
    public int getContentLength() {
        return getIntHeader(Http.Headers.Content_Length);
    }


    /**
     *
     * @return
     */
    @Override
    public String getContentType() {
        return contentType;
    }


    /**
     *
     * @param name  a String specifying the name of the parameter
     * @return
     */
    @Override
    public String getParameter(String name) {
        return  (String)parameters.getValue( name);
    }


    /**
     *
     * @return
     */
    @Override
    public Enumeration getParameterNames() {
        return Collections.enumeration(parameters.getKeys());
    }


    /**
     *
     * @param name a String containing the name of the parameter whose value is requested
     * @return
     */
    @Override
    public String[] getParameterValues(String name) {
        return parameters.getValuesAsArray( name);
    }


    /**
     *
     * @return
     */
    @Override
    public String getProtocol() {
        return "http";
    }


    /**
     *
     * @return
     */
    @Override
    public String getScheme() {
        if( requestURI == null ) return null;
        return requestURI.getScheme();
    }


    /**
     *
     * @return
     */
    @Override
    public String getServerName() {
        if( getHeader( Http.Headers.Host) != null ) {
            String[] hostParts = getHeader( Http.Headers.Host).split(":");
            if( hostParts.length > 0 ) return hostParts[0];
        }

        return getLocalName();
    }


    /**
     *
     * @return
     */
    @Override
    public int getServerPort() {
        if( getHeader( Http.Headers.Host) != null ) {
            String[] hostParts = getHeader( Http.Headers.Host).split(":");
            try {
                if( hostParts.length > 1 ) return Integer.parseInt(hostParts[1]);
            } catch ( NumberFormatException nex){
                LOG.log(Level.WARNING, "Unable to parse port of the Host header " + getHeader( Http.Headers.Host), nex);
            }
        }

        return getLocalPort();
    }


    /**
     *
     * @return
     */
    @Override
    public String getRemoteAddr() {
        return remoteAddr;
    }


    /**
     *
     * @param remoteAddr
     */
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }


    /**
     *
     * @return
     */
    @Override
    public String getRemoteHost() {
        return this.remoteHost;
    }


    /**
     *
     * @param remoteHost
     */
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }


    /**
     *
     * @return
     */
    @Override
    public int getRemotePort() {
        return this.remotePort;
    }


    /**
     *
     * @param remotePort
     */
    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }


    /**
     *
     * @return
     */
    @Override
    public String getLocalName() {
        return this.localHost;
    }


    /**
     *
     * @param localName
     */
    public void setLocalName( String localName ){
        this.localHost = localName;
    }


    /**
     *
     * @return
     */
    @Override
    public String getLocalAddr() {
        return this.localAddr;
    }


    /**
     *
     * @param localAddr
     */
    public void setLocalAddr(String localAddr) {
        this.localAddr = localAddr;
    }


    /**
     *
     * @return
     */
    @Override
    public int getLocalPort() {
        return this.localPort;
    }


    /**
     *
     * @param localPort
     */
    public void setLocalPort(int localPort){
        this.localPort = localPort;
    }


    /**
     *
     * @return
     */
    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }


    /**
     *
     * @param servletContext
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }


    /**
     *
     * @return
     */
    public Http.Version getVersion() {
        return version;
    }


    /**
     *
     * @param version
     */
    public void setVersion(Http.Version version) {
        if( version == null || Http.Version.HTTP_0_9.equals( version ) ){
            throw new ServerRuntimeException( HttpResponse.SC_HTTP_VERSION_NOT_SUPPORTED, "HTTP Version Not Supported");
        }
        this.version = version;
    }


    /**
     *
     * @return
     */
    public MultiMap getHeaders() {
        return headers;
    }


    /**
     *
     * @param headers
     */
    public void setHeaders(MultiMap headers) {
        if( headers == null ) throw new IllegalArgumentException("Null headers in request are not allowed");
        this.headers = headers;
        processHeaders();
    }


    /**
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append( getRemoteAddr() ).append(' ');
        res.append( method.name() ).append(' ');
        if( requestURI != null) res.append( requestURI.toString() ).append(' ');
        if( version != null ) res.append( version.name() ).append(' ');
        return res.toString();

    }


    /**
     *
     */
    public void dump() {
        LOG.finer("Request dump " + toString());
    }


    /**
     *
     */
    protected  void processHeaders() {
        String rawContentType = (String) headers.getActualValue( Http.Headers.Content_Type.toLowerCase());

        if(  rawContentType != null ) {
            String[] values = rawContentType.split(";");

            if( values.length > 1 ) {
                if( values[1].trim().toLowerCase().startsWith("charset=") ) {
                    try {
                        setCharacterEncoding(values[1].trim().toLowerCase().substring(8));
                    } catch (UnsupportedEncodingException e) {
                        LOG.log(Level.WARNING, "Incorrect character set in content type headers " + contentType, e);
                    }
                }
                contentType = values[0].trim();
            } else if ( values.length == 1 ) {
                contentType = values[0].trim();
            } else {
                contentType = rawContentType;
            }
        }
    }
}
