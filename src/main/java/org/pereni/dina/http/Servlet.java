package org.pereni.dina.http;


/**
 * Interface for a small Java program that runs inside the Dina web server. It is at this point a subset of the
 * Java Servlet API.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public interface Servlet {


    /**
     * Called by the servlet container to allow the servlet to respond to a request.
     *
     * @param request the request object that contains the details of the clients request
     * @param response the response object that contains the details of the response
     */
    public void service(HttpRequest request, HttpResponse response);


    /**
     * Called by the servlet container to indicate to a servlet that the servlet is being taken out of service.
     *
     * <p>This method gives the servlet an opportunity to clean up any resources that are being held (for example,
     * memory, file handles, threads).</p>
     */
    public void destroy();
}
