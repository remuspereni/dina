package org.pereni.dina.http;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Defines an object to provide client request information to a servlet. It is a subset of the Java HttpServletRequest
 * interface.
 * The servlet container creates a HttpRequest object and passes it as an argument to the servlet's service method.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public interface HttpRequest {

    /**
     * Returns the value of the specified request header as a String. If the request did not include a header of the
     * specified name, this method returns null. If there are multiple headers with the same name, this method returns
     * the first head in the request. The header name is case insensitive. You can use this method with any request
     * header.
     *
     * @param name the header name
     * @return the value of the requested header, or null if the request does not have a header of that name
     */
    public String getHeader(String name);


    /**
     * Returns all the values of the specified request header as an Enumeration of String objects.
     * If the request did not include any headers of the specified name, this method returns an empty Enumeration.
     * The header name is case insensitive. You can use this method with any request header.
     *
     * @param name the header name
     * @return an Enumeration containing the values of the requested header. If the request does not have any headers
     * of that name return an empty enumeration.
     */
    public Enumeration getHeaders(String name);


    /**
     * Returns an enumeration of all the header names this request contains. If the request has no headers,
     * this method returns an empty enumeration.
     *
     * @return an enumeration of all the header names sent with this request; if the request has no headers,
     * an empty enumeration;
     */
    public Enumeration getHeaderNames();


    /**
     * Returns the value of the specified request header as an int. If the request does not have a header of the
     * specified name, this method returns -1.
     * If the header cannot be converted to an integer, this method throws a NumberFormatException.
     *
     * @param name
     * @return
     */
    public int getIntHeader(String name);


    /**
     * Returns the name of the HTTP method with which this request was made, for example, GET, POST, or PUT.
     *
     * @return the name of the method with which this request was made
     */
    public String getMethod();


    /**
     * Returns any extra path information associated with the URL the client sent when it made this request.
     * The extra path information follows the servlet path but precedes the query string and will start
     * with a "/" character.
     *
     * @return a String, decoded by the web container, specifying extra path information that comes after the servlet
     * path but before the query string in the request URL; or null if the URL does not have any extra path information
     */
    public String getPathInfo();


    /**
     * Returns any extra path information after the servlet name but before the query string, and translates it to a
     * real path.
     *
     * @return a String specifying the real path, or null if the URL does not have any extra path information
     */
    public String getPathTranslated();


    /**
     * Returns the query string that is contained in the request URL after the path. This method returns null if the
     * URL does not have a query string.
     *
     * @return a String containing the query string or null if the URL contains no query string.
     * The value is not decoded by the container.
     */
    public String getQueryString();


    /**
     * Returns the part of this request's URL from the protocol name up to the query string in the first line of the
     * HTTP request. The web container does not decode this String.
     *
     * @return a String containing the part of the URL from the protocol name up to the query string
     */
    public String getRequestURI();


    /**
     * Reconstructs the URL the client used to make the request. The returned URL contains a protocol, server name,
     * port number, and server path, but it does not include query string parameters.
     *
     * @return a StringBuffer object containing the reconstructed URL
     */
    public StringBuffer getRequestURL();


    /**
     * Returns the name of the character encoding used in the body of this request. This method returns "utf-8" if the
     * request does not specify a character encoding
     *
     * @return a String containing the name of the character encoding, or "utf-8" if the request does not specify a
     * character encoding
     */
    public String getCharacterEncoding();


    /**
     * Overrides the name of the character encoding used in the body of this request. This method must be called prior
     * to reading request parameters or reading input using getReader(). Otherwise, it has no effect.
     *
     * @param env the name of the character encoding
     * @throws java.io.UnsupportedEncodingException if this HttpRequest is still in a state where a character
     * encoding may be set, but the specified encoding is invalid
     */
    public void setCharacterEncoding(String env) throws java.io.UnsupportedEncodingException;


    /**
     * Returns the length, in bytes, of the request body and made available by the input stream, or -1 if the length
     * is not known.
     *
     * @return an integer containing the length of the request body or -1 if the length is not known
     */
    public int getContentLength();


    /**
     * Returns the MIME type of the body of the request, or null if the type is not known.
     *
     * @return a String containing the name of the MIME type of the request, or null if the type is not known
     */
    public String getContentType();


    /**
     * Returns the value of a request parameter as a String, or null if the parameter does not exist.
     * Request parameters are extra information sent with the request. For HTTP servlets, parameters are contained in
     * the query string or posted form data.
     *
     * @param name  a String specifying the name of the parameter
     * @return a String representing the single value of the parameter
     */
    public String getParameter(String name);


    /**
     * Returns an Enumeration of String objects containing the names of the parameters contained in this request.
     * If the request has no parameters, the method returns an empty Enumeration.
     *
     * @return an Enumeration of String objects, each String containing the name of a request parameter;
     * or an empty Enumeration if the request has no parameters
     */
    public Enumeration getParameterNames();


    /**
     * Returns an array of String objects containing all of the values the given request parameter has, or null if the
     * parameter does not exist.
     *
     * @param name a String containing the name of the parameter whose value is requested
     * @return an array of String objects containing the parameter's values
     */
    public String[] getParameterValues(String name);


    /**
     * Returns the name and version of the protocol the request uses.
     * @return a String containing the protocol name and version number
     */
    public String getProtocol();


    /**
     * Returns the name of the scheme used to make this request, for example, http, https, or ftp. Different schemes
     * have different rules for constructing URLs, as noted in RFC 1738.
     *
     * @return a String containing the name of the scheme used to make this request
     */
    public String getScheme();


    /**
     * Returns the host name of the server to which the request was sent. It is the value of the part before ":" in
     * the Host header value, if any, or the resolved server name, or the server IP address.
     *
     * @return a String containing the name of the server
     */
    public String getServerName();


    /**
     * Returns the port number to which the request was sent. It is the value of the part after ":" in the Host header
     * value, if any, or the server port where the client connection was accepted on.
     *
     * @return an integer specifying the port number
     */
    public int getServerPort();


    /**
     * Returns the Internet Protocol (IP) address of the client or last proxy that sent the request.
     *
     * @return a String containing the IP address of the client that sent the request
     */
    public String getRemoteAddr();


    /**
     * Returns the fully qualified name of the client or the last proxy that sent the request. If the engine cannot
     * or chooses not to resolve the hostname (to improve performance), this method returns the dotted-string form of
     * the IP address.
     *
     * @return a String containing the fully qualified name of the client
     */
    public String getRemoteHost();


    /**
     * Returns the Internet Protocol (IP) source port of the client or last proxy that sent the request.
     *
     * @return an integer specifying the port number
     */
    public int getRemotePort();


    /**
     * Returns the host name of the Internet Protocol (IP) interface on which the request was received.
     *
     * @return a String containing the host name of the IP on which the request was received.
     */
    public String getLocalName();


    /**
     * Returns the Internet Protocol (IP) address of the interface on which the request was received.
     *
     * @return a String containing the IP address on which the request was received.
     */
    public String getLocalAddr();


    /**
     *  Returns the Internet Protocol (IP) port number of the interface on which the request was received.
     *
     * @return an integer specifying the port number
     */
    public int getLocalPort();


    /**
     * Gets the servlet context to which this HttpRequest was last dispatched.
     *
     * @return the servlet context to which this HttpRequest was last dispatched
     */
    public ServletContext getServletContext();

}
