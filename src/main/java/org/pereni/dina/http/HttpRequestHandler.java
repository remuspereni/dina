package org.pereni.dina.http;


import org.pereni.dina.server.AbstractRequestHandler;
import org.pereni.dina.server.ServerRuntimeException;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Arrays;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides a Java Nio based network event based dispatcher implementation for Http based traffic
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class HttpRequestHandler extends AbstractRequestHandler {

    /**
     * Class logger
     */
    private static final Logger LOG = Logger.getLogger( HttpRequestHandler.class.getName());

    /**
     * Default size for the input buffer that will be used for the lexer
     */
    private static final int INPUT_BUFFER_SIZE = 16 * 1024;

    /**
     * Input buffer used for the lexer
     */
    private ByteBuffer inputBuffer = ByteBuffer.allocate( INPUT_BUFFER_SIZE );

    /**
     * The HttpRequest instance that is going to be filled by the handler and passed to the servlet
     */
    private HttpRequestImpl request = new HttpRequestImpl();

    /**
     * The HttpResponse that is going to be filled by the servlet and passed to the handler to be sent to the client
     */
    private HttpResponseImpl response = new HttpResponseImpl();

    /**
     * Current state in the request processing state machine
     */
    private StateHandler currentStateHandler = new StartStateHandler();

    /**
     * The request context shared with the state handlers
     */
    private RequestContext requestContext = new RequestContext();

    /**
     * Flag signaling if the connection is keep alive or not
     */
    private boolean isKeepAlive = false;

    /**
     *  The default idle time after which the keep alive connection will be automatically closed
     */
    private static final int KEEP_ALIVE_DEFAULT = 5; // 5 seconds

    /**
     *  The idle time after which the keep alive connection will be automatically closed
     */
    private int keepAliveTimeOut = KEEP_ALIVE_DEFAULT;

    /**
     * Lexer instance parsing the request headers
     */
    private Iterator<HttpLexer.Token> httpLexer;

    /**
     * Currently selected token in the lexer
     */
    private HttpLexer.Token lexerCurretToken;

    /**
     * The instance of the http dispatcher
     */
    private HttpDispatcher httpDispatcher;

    /**
     * Counter for the number of requested served
     */
    private int nrRequestsServed = 0;

    /**
     * The actual connection socket
     */
    private Socket socket;

    /**
     * The timestamp of the last response, used for determining in a keep alive connection if it is the time to
     * close the connection.
     */
    private long lastResponseTimestamp = -1;

    /**
     * The possible requests states
     */
    public static enum RequestState {

        /**
         * Request is not yet processed, we are at the start
         */
          Start

        /**
         * Processing the request headers
         */
        , ProcessingHeaders

        /**
         * Ready to process the request body
         */
        , ProcessingBody

        /**
         * All the request input has been consumed, ready to compute the output
         */
        , ReadyToRespond

        /**
         * The response has been computed in process of sending it to the client
         */
        , SendingResponse

        /**
         * Response has been successfuly sent to the client
         */
        , SuccessResponse

        /**
         * Failure durring request processing
         */
        , FailureResponse

        /**
         * The request processing has been finalized, with error or success
         */
        , Completed
    }


    /**
     * Simple context to be shared between the request states.
     */
    private class RequestContext {

        public ReadableByteChannel inputChannel;
        public WritableByteChannel outputChannel;

        public HttpRequestHandler requestHandler;
        public HttpRequestImpl request;
        public HttpResponseImpl response;

        public int statusCode = -1;
        public String statusMessage;
    }


    /**
     * A state implementation in a state machine handling the various states and operations related to processing
     * a request.
     */
    public class StateHandler {


        /**
         * Handle the state of the request.
         * @param context the context that sums the details of the request
         */
        public void handleState(RequestContext context) {
            LOG.log(Level.FINER, "New request state {0} handler {1}"
                    , new String[]{ getHandledState().name(), HttpRequestHandler.this.toString()} );
        }


        /**
         * Return the state that the current state handler handles
         * @return the state that the current state handler handles
         */
        public RequestState getHandledState() {
            return null;
        }
    }


    /**
     * Default constructor.
     * @param dispatcher the instance of the dispatcher handling the events
     * @param socket the instance of the connection socket
     */
    public HttpRequestHandler(HttpDispatcher dispatcher, Socket socket){
        super(dispatcher);
        this.socket = socket;
        this.httpDispatcher = dispatcher;
        requestContext.request = request;
        requestContext.response = response;
        requestContext.requestHandler = this;
    }


    /**
     * Handle the client supplied input
     */
    @Override
    public synchronized void handleInput() {

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Handle input " + toString());
        }

        if( getInputChannel() == null ) {
            throw new IllegalArgumentException("Input channel of byte buffer is null");
        }
        if( !getInputChannel().isOpen() ) {
            throw new IllegalStateException("Input channel is already closed or has not been opened yed");
        }

        requestContext.inputChannel = getInputChannel();

        try {
            getCurrentStateHandler().handleState(requestContext);
        } catch ( ServerRuntimeException se) {
            LOG.log( Level.SEVERE, " Exception while processing response", se);
            handleFailure( se.getExceptionCode(), se.getMessage());
        } catch (Exception ex) {
            LOG.log( Level.SEVERE, " Exception while processing response", ex);
            handleFailure(HttpResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error");
        }

        inputBuffer.clear();

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Handle input exit " + toString());
        }
    }


    /**
     * The servlet produced output, handle it
     */
    @Override
    public synchronized void handleOutput() {

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Handle output " + toString());
        }

        if( !isReadyForOutput() ) return;

        if( RequestState.ReadyToRespond.equals( getCurrentStateHandler().getHandledState()) ) {
            setCurrentStateHandler(new SendingResponseStateHandler());
        }

        try {
            getCurrentStateHandler().handleState(requestContext);
        } catch ( ServerRuntimeException se) {
            LOG.log( Level.SEVERE, " Exception while processing response", se);
            handleFailure( se.getExceptionCode(), se.getMessage());

        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.log( Level.SEVERE, " Exception while processing response", ex);
            handleFailure(HttpResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error");
        }

        if( LOG.isLoggable(Level.FINE) ) {
            LOG.fine("Handle output exit " + toString());
        }
    }


    /**
     * Return true if the request is handled as keep alive, false otherwise
     * @return true if the request is handled as keep alive, false otherwise
     */
    public boolean isKeepAlive() {
        return isKeepAlive;
    }


    /**
     * Set the request keep alive statsu
     * @param keepAlive true if the request should be handled as keep alive, false otherwise
     */
    private void setKeepAlive(boolean keepAlive) {
        this.isKeepAlive = keepAlive;
    }


    /**
     * Return the keep alive timeout, either as set by the default value or supplied the the request in the keep alive
     * header.
     *
     * @return the keep alive timeout, either as set by the default value or supplied the the request in the keep alive
     * header.
     */
    public int getKeepAliveTimeOut() {
        return keepAliveTimeOut;
    }


    /**
     * Set the keep alive timeout
     * @param keepAliveTimeOut the keep alive timeout
     */
    public void setKeepAliveTimeOut(int keepAliveTimeOut) {
        this.keepAliveTimeOut = keepAliveTimeOut;
    }


    /**
     * Hanlde request failure
     * @param errorCode the error code of the response
     * @param errorMessage the error message of the response
     */
    private void handleFailure ( int errorCode, String errorMessage) {
        LOG.severe("Handling failure " + errorCode );
        setReadyForOutput(true);

        requestContext.statusCode = errorCode;
        requestContext.statusMessage = errorMessage;


        setCurrentStateHandler(new FailureResponseStateHandler());
        getCurrentStateHandler().handleState( requestContext );

        LOG.log( Level.SEVERE, "Exception during request processing, error code is " + errorCode
                + " error message " + errorMessage);
    }


    /**
     * Return the current state handler.
     *
     * @return the current state handler.
     */
    public StateHandler getCurrentStateHandler() {
        return currentStateHandler;
    }


    /**
     * Set the current state handler instance
     * @param currentStateHandler the current state handler
     */
    public void setCurrentStateHandler(StateHandler currentStateHandler) {
        this.currentStateHandler = currentStateHandler;
    }


    /**
     * Return the current request instance
     * @return the current request instance
     */
    public HttpRequestImpl getRequest() {
        return request;
    }


    /**
     *
     * @param outputChannel The output channel on which the output can be performed
     */
    @Override
    public void setOutputAvailable(WritableByteChannel outputChannel) {
        super.setOutputAvailable(outputChannel);
        if( response != null ) {
            response.setOutputChannel(outputChannel);
        }
    }


    /**
     *
     * @return
     */
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("{ id:'").append(hashCode()).append("'");
        result.append(", th:'").append( Thread.currentThread().getName()).append("'");
        result.append(", isWrk:").append(isWorking.get());
        result.append(", isInAv:").append(isInputAvailable());
        result.append(", isOutAv:").append(isOutputAvailable());
        result.append(", isROu:").append(isReadyForOutput());
        result.append(", isRCls:").append(isReadyForClose());
        result.append(", isKA:").append(isKeepAlive);
        result.append(", kaTO:").append(keepAliveTimeOut);
        result.append(", kaNum:").append(nrRequestsServed);
        result.append(", state:'").append(currentStateHandler.getHandledState().name()).append("'");
        result.append("}");
        return result.toString();
    }


    /**
     *
     * @return
     */
    @Override
    public boolean isInValid() {
        return  RequestState.Start.equals( currentStateHandler.getHandledState())
                && !isWorking()
                && lastResponseTimestamp != -1
                && (System.currentTimeMillis() - lastResponseTimestamp >= KEEP_ALIVE_DEFAULT * 1000);
    }


    /**
     *
     */
    @Override
    public void destroy() {
        if( requestContext.inputChannel != null ) try { requestContext.inputChannel.close(); } catch (Exception ex){}
        if( requestContext.outputChannel != null ) try { requestContext.outputChannel.close(); } catch (Exception ex){}
        httpDispatcher.deRegisterHandler(this);
    }


    /**
     * Provides the logic to start processing a new request, focused on the request line
     */
    private class StartStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);


            if( socket != null &&  socket.getRemoteSocketAddress()  instanceof InetSocketAddress ) {
                InetSocketAddress netSocket = (InetSocketAddress) socket.getRemoteSocketAddress();
                context.request.setRemoteAddr( netSocket.getAddress().getHostAddress());
                context.request.setRemotePort( netSocket.getPort());
                context.request.setRemoteHost( netSocket.getHostName());
            }

            if( socket != null &&  socket.getLocalSocketAddress()  instanceof InetSocketAddress ) {
                InetSocketAddress netSocket = (InetSocketAddress) socket.getLocalSocketAddress();
                context.request.setLocalAddr( netSocket.getAddress().getHostAddress());
                context.request.setLocalPort( netSocket.getPort());
                context.request.setLocalName( netSocket.getHostName());
            }

            try {
                int count = context.inputChannel.read( inputBuffer );
                LOG.finest("Start state read " + count + " bytes from the input channel");

                // check if the socket was closed
                if( count == -1 ) {
                    context.inputChannel.close();
                    if( context.outputChannel != null ) context.outputChannel.close();
                    setReadyForClose(true);
                    return;
                }

                inputBuffer.flip();		// make buffer readable

                // loop while data available, channel is non-blocking
                while ( count > 0) {                                        // input is not complete yet, read it

                    httpLexer = new HttpLexer(Arrays.copyOf( inputBuffer.array(), inputBuffer.limit()), lexerCurretToken);

                    if( httpLexer.hasNext() ) {
                        lexerCurretToken = httpLexer.next();

                        // check if we have a next request on the same pipe from Keep Alive
                        if( HttpLexer.TokenType.EOF.equals( lexerCurretToken.Type)) {
                            LOG.finest("Found EOF at the biggining probably a keep alive channel");
                            if( httpLexer.hasNext() ) {
                                lexerCurretToken = httpLexer.next();
                                if( httpLexer.hasNext() )  lexerCurretToken = httpLexer.next();

                                LOG.finest("Next toke is of type " + lexerCurretToken.Type.name()
                                        + "  and value starts with " + lexerCurretToken.Value.toString());


                            } else {
                                lexerCurretToken = null;
                            }

                        }

                        if( lexerCurretToken != null && HttpLexer.TokenType.LITERAL.equals( lexerCurretToken.Type)  ) {

                            // check to see if we have the complete request line. If it is not it means that we only
                            // get the beginning so we will wait until the full line comes.
                            if( !lexerCurretToken.IsComplete ) return;

                            String[] elements = lexerCurretToken.Value.toString().split("\\s+");

                            if( elements.length < 3 ) {
                                LOG.warning("Request line has " + elements.length
                                        + " elements, expected 3. Value is "
                                        + lexerCurretToken.Value.toString() );
                                throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
                            }

                            Http.Method method = Http.Method.valueOf(elements[0]);
                            context.request.setMethod(method);

                            URI uri = new URI(elements[1]);
                            context.request.setRequestURI(uri);

                            LOG.finer("New request on " + uri.getPath() + " of type " + method.name());

                            Http.Version version = Http.Version.valueOf(
                                    elements[2].replace('/', '_').replace('.', '_'));
                            context.request.setVersion( version);

                        } else {
                            // it does not start with a potential request line, not valid http request
                            LOG.warning("Unable to understand request it is of type " + lexerCurretToken.Type.name()
                                        + "  and value starts with " + lexerCurretToken.Value.toString());
                            throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
                        }
                    }

                    // we've got the request line now we need to find the EOL so we can move to the headers
                    if( httpLexer.hasNext() ) {

                        lexerCurretToken = httpLexer.next();

                        if( HttpLexer.TokenType.EOL.equals( lexerCurretToken.Type) ) {
                            context.requestHandler.setCurrentStateHandler(new ProcessingHeadersStateHandler());
                            context.requestHandler.getCurrentStateHandler().handleState(context);
                        } else {
                            LOG.warning("Unable to understand request it does not end properly, expecting EOL found  "
                                    + lexerCurretToken.Value.toString());
                            throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
                        }
                        return;
                    }


                    inputBuffer.clear();		// make buffer empty
                    count = context.inputChannel.read( inputBuffer);
                }

            } catch (IOException ex) {
                LOG.log(Level.SEVERE, "Unable to process request input", ex);
            } catch (URISyntaxException e) {
                LOG.log(Level.SEVERE, "Unable to process request input", e);
                throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad Request");
            }
        }


        @Override
        public RequestState getHandledState() {
            return RequestState.Start;
        }
    }


    /**
     * Provides the logic to process the request headers
     */
    private class ProcessingHeadersStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            // Verify first the lexer, maybe the lexer already had a buffer and parsed the content of some
            // headers too;

            String headerName = null ;
            String headerValue = null ;
            boolean isHeaderName = true;

            if( httpLexer != null && httpLexer.hasNext() ) {


                while( httpLexer.hasNext() ) {

                    lexerCurretToken = httpLexer.next();

                    // Header not completely read, return, let the buffer load some more first
                    if( !lexerCurretToken.IsComplete ) return;

                    switch ( lexerCurretToken.Type ){

                        case LITERAL:
                            if( isHeaderName ) headerName = lexerCurretToken.Value.toString();
                            else headerValue = lexerCurretToken.Value.toString();
                            break;


                        case SEPARATOR:
                            isHeaderName = false;
                            break;

                        case EOL:
                            if( headerName != null && headerValue != null ) {
                                context.request.getHeaders().put( headerName.trim().toLowerCase(), headerValue.trim());
                                LOG.finer("Found header " + headerName + " / " + headerValue);
                            } else {
                                LOG.warning("End of line parsing the headers yet missing the complete key value pair"
                                + " key is " + headerName + " value is " + headerValue);
                                throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
                            }

                            headerName = null;
                            headerValue = null;
                            isHeaderName = true;
                            break;

                        case EOF:
                            if( headerName != null && headerValue != null ) {
                                context.request.getHeaders().put( headerName.trim().toLowerCase(), headerValue.trim());
                                LOG.finer("Found header " + headerName + " / " + headerValue);
                            } else {
                                LOG.warning("End of line parsing the headeers yet missing the complete key value pair"
                                        + " key is " + headerName + " value is " + headerValue);
                                throw new ServerRuntimeException( HttpResponse.SC_BAD_REQUEST, "Bad request");
                            }

                            LOG.finer("End of headers found, mooving to body");
                            context.requestHandler.setCurrentStateHandler(new ProcessingBodyStateHandler());
                            context.requestHandler.getCurrentStateHandler().handleState(context);
                            return;
                    }
                }

            }
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.ProcessingHeaders;
        }
    }


    /**
     * Provides the logic to process the request body
     */
    private class ProcessingBodyStateHandler extends StateHandler {
        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            request.processHeaders();

            if( Http.ContentType.UrlEncoded.equalsIgnoreCase(request.getContentType())) {
                LOG.finer(" Url encoded form post found getting the parameters for processing ");

                String body = null;

                if( httpLexer != null && httpLexer.hasNext() ) {

                    body = httpLexer.next().Value.toString();
                    LOG.finer("Body content is " + httpLexer.next().Type + " value " + body);
                    if( body != null ) request.processQueryParameters(body);
                }
            }

            String connectionValue = context.request.getHeader( Http.Headers.Connection );

            if( context.request.getVersion().equals(Http.Version.HTTP_1_1)
                || ( connectionValue != null )) { //&& connectionValue.contains( Http.Connection.Keep_Alive) )){

                setKeepAlive( true );

                if( context.request.getHeader( Http.Headers.Keep_Alive )  != null ) {
                    // TODO: Read the timeout and max values here and set them on the connection
                }
            } else {
                setKeepAlive( false );
            }

            context.requestHandler.setCurrentStateHandler(new ReadyToRespondStateHandler());
            context.requestHandler.getCurrentStateHandler().handleState(context);
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.ProcessingBody;
        }
    }


    /**
     * Requst state able to prepare the handler for the response
     */
    private class ReadyToRespondStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            if( isKeepAlive() ) {
                response.addHeader( Http.Headers.Connection, Http.Connection.Keep_Alive);

                if( getKeepAliveTimeOut() != -1 ) {
                    response.addHeader( Http.Connection.Keep_Alive, Http.Connection.Time_Out + "= " + getKeepAliveTimeOut() );
                }
            } else {
                response.addHeader( Http.Headers.Connection, Http.Connection.Closed);
            }

            if( Http.Method.HEAD.name().equals( request.getMethod())) {
                response.setHeaderOnlyResponse(true);
            }

            ServletContext servletContext = httpDispatcher.getServletContext();

            if( servletContext != null ) {

                context.request.setServletContext( servletContext);
                context.response.setServletContext( servletContext);


            } else {
                LOG.log(Level.SEVERE, "Servlet context in dispatcher is null ");
            }

            setReadyForOutput(true);
        }


        @Override
        public RequestState getHandledState() {
            return RequestState.ReadyToRespond;
        }
    }


    /**
     * State handler that processes that invokes the servlet and processes the response
     */
    private class SendingResponseStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            LOG.finer("Servlet context is " + context.request.getServletContext());
            Servlet servlet = context.request.getServletContext().instantiateServlet( context.request.getRequestURI());

            if( servlet != null ) {
                servlet.service( context.request, context.response);
                servlet.destroy();
                LOG.finer("Servlet successfully invoked and destroyed");
            } else {
                LOG.log( Level.SEVERE, "Unable to instantiate servlet");
                try {
                    context.response.sendError( HttpResponse.SC_NOT_FOUND, "Page not found");
                } catch (IOException e) {
                    LOG.log( Level.WARNING, "Exception while sending page not found error", e);
                }
            }

            context.response.finalizeResponse();
            context.requestHandler.setCurrentStateHandler(new SuccessResponseStateHandler());
            context.requestHandler.getCurrentStateHandler().handleState(context);
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.SendingResponse;
        }
    }


    /**
     * Success state handler
     */
    private class SuccessResponseStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            context.requestHandler.setCurrentStateHandler(new CompletedStateHandler());
            context.requestHandler.getCurrentStateHandler().handleState(context);
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.SuccessResponse;
        }
    }


    /**
     * Failure state handler
     */
    private class FailureResponseStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            if( !context.response.isCommitted() ) {

                if( context.statusCode != -1 ) {

                    try {
                        if( context.statusMessage != null ) {
                            context.response.sendError( context.statusCode, context.statusMessage);
                        } else {
                            context.response.sendError( context.statusCode);
                        }
                    } catch (IOException ex) {
                        LOG.log(Level.WARNING, "Exception while send error response to the client", ex);
                    }
                }
            } else {
                LOG.warning("Response already comminted, can not send error message");
            }

            context.response.finalizeResponse();
            context.requestHandler.setCurrentStateHandler(new CompletedStateHandler() );
            context.requestHandler.getCurrentStateHandler().handleState(context);
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.FailureResponse;
        }
    }


    /**
     * Response finalized state handler.
     */
    private class CompletedStateHandler extends StateHandler {

        @Override
        public void handleState(RequestContext context) {
            super.handleState(context);

            LOG.info(context.request.toString() + " " + context.response.toString());

            setReadyForOutput(false);

            if( isKeepAlive() ) {
                setReadyForClose(false);

                LOG.finer("Keep alive request re-initializing request and response");
                request = context.request = new HttpRequestImpl();
                response = context.response = new HttpResponseImpl();
                currentStateHandler = new StartStateHandler();
                nrRequestsServed++;

            } else {
                setReadyForClose(true);
                destroy();
            }

            lastResponseTimestamp = System.currentTimeMillis();
        }

        @Override
        public RequestState getHandledState() {
            return RequestState.Completed;
        }
    }
}
