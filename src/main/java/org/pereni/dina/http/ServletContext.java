package org.pereni.dina.http;

/**
 * Provides a bridge between a <code>Servlet</code> and the servlet container, for example to get the real path
 * of a resource, the mime type or to get the mapped <code>Servlet</code> instance on a specific request URL.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public interface ServletContext {

    /**
     * Gets the real path corresponding to the given request uri path.
     *
     * @param path the virtual path to be translated to a real path
     * @return the real path, or null if the translation cannot be performed
     */
    public String getRealPath(String path);

    /**
     * Returns the MIME type of the specified file, or null if the MIME type is not known.
     * The MIME type is determined by the configuration of the servlet container, and may be specified in a
     * web application deployment descriptor. Common MIME types include text/html and image/gif.
     *
     * @param file the name of a file
     * @return the file's MIME type
     */
    public String getMimeType(String file);


    /**
     * Create a new <code>Servlet</code> instance that will handle the response of a request with the URI given as
     * parameter.
     *
     * @param requestURI the uri that will be used to determine the servlet handling the response
     * @return the servlet instance that will handle the request or null of there is no servlet instance mapped on the
     * request uri.
     */
    public Servlet instantiateServlet(String requestURI);
}
