package org.pereni.dina.http;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.channels.WritableByteChannel;


/**
 * Defines an object to assist a servlet in sending a response to the client. The servlet container creates a
 * HttpResponse object and passes it as an argument to the servlet's service method.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public interface HttpResponse {

    /**
     * Status code (200) indicating the request succeeded normally.
     */
    public static final int SC_OK = 200;

    /**
     * Status code (204) indicating that the request succeeded but that there was no new information to return.
     */
    public static final int SC_NO_CONTENT = 204;

    /**
     * Status code (304) indicating that a conditional GET operation found that the resource was available and not
     * modified.
     */
    public static final int SC_NOT_MODIFIED = 304;

    /**
     * Status code (400) indicating the request sent by the client was syntactically incorrect.
     */
    public static final int SC_BAD_REQUEST = 400;

    /**
     * Status code (401) indicating that the request requires HTTP authentication.
     */
    public static final int SC_UNAUTHORIZED = 401;

    /**
     * Status code (403) indicating the server understood the request but refused to fulfill it.
     */
    public static final int SC_FORBIDDEN = 403;

    /**
     * Status code (404) indicating that the requested resource is not available.
     */
    public static final int SC_NOT_FOUND = 404;

    /**
     * Status code (405) indicating that the method specified in the Request-Line is not allowed for the
     * resource identified by the Request-URI.
     */
    public static final int SC_METHOD_NOT_ALLOWED = 405;

    /**
     * Status code (406) indicating that the resource identified by the request is only capable of generating response
     * entities which have content characteristics not acceptable according to the accept headers sent in the request.
     */
    public static final int SC_NOT_ACCEPTABLE = 406;

    /**
     * Status code (408) indicating that the client did not produce a request within the time that the server was
     * prepared to wait.
     */
    public static final int SC_REQUEST_TIMEOUT = 408;

    /**
     * Status code (415) indicating that the server is refusing to service the request because the entity of the
     * request is in a format not supported by the requested resource for the requested method.
     */
    public static final int SC_UNSUPPORTED_MEDIA_TYPE = 415;

    /**
     * Status code (500) indicating an error inside the HTTP server which prevented it from fulfilling the request.
     */
    public static final int SC_INTERNAL_SERVER_ERROR = 500;

    /**
     * Status code (501) indicating the HTTP server does not support the functionality needed to fulfill the request.
     */
    public static final int SC_NOT_IMPLEMENTED = 501;

    /**
     * Status code (503) indicating that the HTTP server is temporarily overloaded, and unable to handle the request.
     */
    public static final int SC_SERVICE_UNAVAILABLE = 503;

    /**
     * Status code (505) indicating that the server does not support or refuses to support the HTTP protocol version
     * that was used in the request message.
     */
    public static final int SC_HTTP_VERSION_NOT_SUPPORTED = 505;


    /**
     * Sends an error response to the client using the specified status and clears the buffer.
     *
     * @param sc the error status code
     * @param msg the descriptive message
     * @throws IOException if an input or output exception occurs
     */
    public void sendError(int sc, String msg) throws IOException;


    /**
     * Sends an error response to the client using the specified status and clears the buffer.
     *
     * @param sc  the error status code
     * @throws IOException if an input or output exception occurs
     */
    public void sendError(int sc) throws IOException;


    /**
     * Sets a response header with the given name and date-value. The date is specified in terms of milliseconds since
     * the epoch. If the header had already been set, the new value overwrites the previous one.
     *
     * @param name the name of the header to set
     * @param date the assigned date value
     */
    public void setDateHeader(String name, long date);


    /**
     * Adds a response header with the given name and date-value. The date is specified in terms of milliseconds since
     * the epoch. This method allows response headers to have multiple values.
     *
     * @param name the name of the header to set
     * @param date the assigned date value
     */
    public void addDateHeader(String name, long date);


    /**
     * Sets a response header with the given name and value. If the header had already been set, the new value
     * overwrites the previous one.
     *
     * @param name the name of the header to set
     * @param value the assigned header value
     */
    public void setHeader(String name, String value);


    /**
     * Adds a response header with the given name and value. This method allows response headers to have multiple values
     *
     * @param name the name of the header to set
     * @param value the assigned header value
     */
    public void addHeader(String name, String value);


    /**
     * Sets a response header with the given name and integer value. If the header had already been set, the new value
     * overwrites the previous one.
     *
     * @param name the name of the header
     * @param value  the assigned integer value
     */
    public void setIntHeader(String name, int value);


    /**
     * Adds a response header with the given name and integer value. This method allows response headers to have
     * multiple values.
     *
     * @param name the name of the header
     * @param value the assigned integer value
     */
    public void addIntHeader(String name, int value);


    /**
     * Sets the status code for this response. This method is used to set the return status code when there is no error.
     * @param sc the status code
     */
    public void setStatus(int sc);


    /**
     * Gets the current status code of this response.
     * @return the current status code of this response
     */
    public int getStatus();


    /**
     * Returns the name of the character encoding (MIME charset) used for the body sent in this response.
     *
     * @return a String specifying the name of the character encoding, for example, UTF-8
     */
    public String getCharacterEncoding();


    /**
     * Returns the content type used for the MIME body sent in this response.
     *
     * @return a String specifying the content type, for example, text/html; charset=UTF-8, or null
     */
    public String getContentType();


    /**
     * Returns a PrintWriter object that can send character text to the client.  The PrintWriter uses the character
     * encoding returned by getCharacterEncoding().
     *
     * @return a PrintWriter object that can return character data to the client
     * @throws IOException if an input or output exception occurred
     */
    public PrintWriter getWriter() throws IOException;


    /**
     * Sets the character encoding (MIME charset) of the response being sent to the client, for example, to UTF-8.
     *
     * @param charset charset - a String specifying only the character set defined by IANA
     */
    public void setCharacterEncoding(String charset);


    /**
     * Sets the length of the content body in the response In HTTP servlets, this method sets the HTTP Content-Length
     * header.
     *
     * @param len an integer specifying the length of the content being returned to the client; sets the Content-Length
     *            header
     */
    public void setContentLength(long len);


    /**
     * Sets the content type of the response being sent to the client, if the response has not been committed yet.
     *
     * @param type a String specifying the MIME type of the content
     */
    public void setContentType(String type);


    /**
     * Returns a boolean indicating if the response has been committed. A committed response has already had its status
     * code and headers written.
     *
     * @return a boolean indicating if the response has been committed
     */
    public boolean isCommitted();

    /**
     * Forces any content in the buffer to be written to the client. A call to this method automatically commits the
     * response, meaning the status code and headers will be written.
     */
    public void flushBuffer();


    /**
     * Gets the servlet context to which this HttpRequest was last dispatched.
     *
     * @return the servlet context to which this HttpRequest was last dispatched
     */
    public ServletContext getServletContext();


    /**
     * Returns a WritableByteChannel object that can send character text to the client.
     *
     * @return the WritableByteChannel used to output content to the client.
     */
    public WritableByteChannel getOutputChannel();
}
