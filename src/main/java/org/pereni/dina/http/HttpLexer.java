package org.pereni.dina.http;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple lexer implementation that would allow to break up the HTTP request and headers
 * into a token stream that can be processed further by the HTTP request handler
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class HttpLexer implements Iterator<HttpLexer.Token> {

    /*
     * Some of the US-ASCII (byte) characters that define the syntax of
     * a HTTP request
     */
    public static final byte SPACE = 0x20;
    public static final byte TAB = 0x09;
    public static final byte LINE_FEED = 0x0A;
    public static final byte CARRIAGE_RETURN = 0x0D;
    public static final byte COLON = (byte) ':';


    /**
     * Class representing a token with the type of the token
     * and the value of the parsed symbols associated
     */
    public class Token {
        public TokenType Type;
        public CharSequence Value;
        public boolean IsComplete = true;
    }

    /**
     * The TokenType enumeration defines the possible values
     * the HTTP request can have at lower level
     */
    public static enum TokenType {

        /**
         * A literal, like the HTTP request line, or the header key or the header value
         */
         LITERAL

        /**
         * End of a HTTP line (ends with CR + LF, in Java that would be \r\n)
         */
        , EOL

        /**
         * End of the header section from the HTTP request, finishes with CR+LF+CR+LF,
         * In Java that would be \r\n\r\n
         */
        , EOF

        /**
         * Potential header line key, value separator, only the first one counts on a line
         * the second one will be considered part of the content
         */
        , SEPARATOR
    }

    private static final Logger LOG = Logger.getLogger(HttpLexer.class.getName());

    /*
     * The input buffer holding the actual content. It might not be complete
     */
    private byte[] inputBuffer;

    /*
     * The position of the cursor in the input buffer
     */
    private int position = 0;

    /*
     * Flag indicating that the end of header section was reached
     * in the input stream following might be the body of the request
     * (if exists)
     */
    private boolean isEOF = false;

    /*
     * Flag indicating that the current COLON key/value separator in the header is the first
     * one on the line or not. If it is the first one it is the separator otherwise it is
     * probably just part of the content
     */
    private boolean isFirstColon = true;

    /*
     * The value of the token being returned as part of the next() request. We could have been
     * creating a new instance for every next request but that would not be very effective
     * If the value is changed outside this lexer during the parsing process the results might
     * be un-predictable
     */
    private Token nextToken = new Token();


    /*
     * We might want to continue the lexing operation on a new buffer in case the request comes
     * in multiple parts.
     */
    private boolean isInitialTokenAvailable = false;


    /**
     * Create an instance of the lexer a complete or partial byte array part of the request
     * @param inputBuffer The complete or partial request
     */
    public HttpLexer(byte[] inputBuffer) {
        if (inputBuffer == null) throw new IllegalArgumentException("Can not process a null byte buffer");
        this.inputBuffer = inputBuffer;
    }


    /**
     * Create an instance of the lexer a complete or partial byte array part of the request
     * @param inputBuffer The complete or partial request
     * @param initialToken The initial token containing the state and info from a previous parsing operation
     *                     that was not finalized
     */
    public HttpLexer(byte[] inputBuffer, Token initialToken) {
        this( inputBuffer );
        if( initialToken != null && !initialToken.IsComplete ) {
            LOG.finest("Continuing lexing with initial token [type/value] = " + initialToken.Type + "/" + initialToken.Value);
            nextToken = initialToken;
            isInitialTokenAvailable = true;
        }
    }

    /**
     * Check if there is a next token to be consumed.
     * @return true if there is a next token to be consumed, false otherwise
     */
    @Override
    public boolean hasNext() {
        return  position < inputBuffer.length;
    }


    /**
     * Scan the input and determine the next token
     * @return the next token identified in the input stream
     */
    @Override
    public Token next() {
        byte nextSymbol = getNextSymbol();
        boolean isNextTokenReady = false;


        // if we have to continue a stream then we start with an initial leftover token in the next token
        // so we don't want to initialize it
        if( position > 0  || !isInitialTokenAvailable ) {
            initializeNextToken();
        }

        while( !isNextTokenReady && nextSymbol != -1 ) {

            if( LOG.isLoggable( Level.FINEST ) ) {
                LOG.finest(" nextSymbol in lexer " + nextSymbol + " char ='" + (char) nextSymbol + "' "
                        + " is CR " + (nextSymbol == CARRIAGE_RETURN)
                        + " is LF " + (nextSymbol == LINE_FEED)
                        + " is new token " + isNewToken()
                        + " isFirstColon " + isFirstColon
                        + " cursor position " + position
                        + " is  EOF " + isEOF);
            }

            switch (nextSymbol) {

                case COLON:

                    // check if it is an active token in line, if the colon is not the first in line
                    // meaning that already was the separator now we are dealing with a content
                    // check if the current token is literal, if it is then add to it, move to next symbol
                    // if it is not then start a new literal token
                    if( !isNewToken() ) {

                        if( !isFirstColon && TokenType.LITERAL.equals( nextToken.Type) ) {
                            consumeSymbol();
                            nextSymbol = getNextSymbol();
                            continue;
                        }

                        isNextTokenReady = true;
                        nextToken.IsComplete = true;
                        continue;
                    }

                    // if we are parsing a new token check if it is the first colon if it is then it is the separator
                    // create a separator toke and move on. If it is not the first token then create a literal
                    // and move on
                    if( isFirstColon ) {
                        nextToken.Type = TokenType.SEPARATOR;
                        consumeSymbol();
                        isFirstColon = false;
                        isNextTokenReady= true;
                    } else {
                        nextToken.Type = TokenType.LITERAL;
                        consumeSymbol();
                    }
                    break;


                case CARRIAGE_RETURN:

                    // do we have a token that has been built?
                    // if yes then please verify if we have continuation
                    // if it is continuation then transform \r\n[ |\t]  into space
                    // and move on with the current token
                    // otherwise just finish the current token it comes an EOL or EOF

                    if ( !isNewToken()
                          && (
                                    !TokenType.EOL.equals(nextToken.Type)
                                ||  !TokenType.EOF.equals(nextToken.Type))) {

                        // we have a literal probably check if we have folding
                        if( lookAhead(1) == LINE_FEED
                                && ( lookAhead(2) == TAB || lookAhead(2) == SPACE )) {
                            skipSymbol(3);
                            nextSymbol = getNextSymbol();
                            ((StringBuilder)nextToken.Value).append((char) SPACE);
                        }  else {
                            nextToken.IsComplete = true;
                            isNextTokenReady = true;
                        }
                        continue;

                    }

                    if (lookAhead(1) == LINE_FEED) {


                        // is it end of headers \r\n\r\n ?
                        if (lookAhead(2) == CARRIAGE_RETURN) {

                            if (lookAhead(3) == LINE_FEED) {
                                skipSymbol(4);
                                nextToken.Type = TokenType.EOF;
                                isEOF = true;
                                isNextTokenReady = true;
                                continue;
                            }
                        }

                        // it seems it is not end of headers so just end of line
                        skipSymbol(2);
                        nextToken.Type = TokenType.EOL;
                        isNextTokenReady = true;
                        isFirstColon = true;
                        continue;
                    }
                    skipSymbol();
                    break;


                        // Literal, consume everything until the next token
                default:
                    if( isNewToken() ){
                        nextToken.Type = TokenType.LITERAL;
                        nextToken.IsComplete = false;
                        consumeSymbol();
                    } else if( TokenType.LITERAL.equals(nextToken.Type) ) {
                        consumeSymbol();
                    } else {
                        isNextTokenReady = true;
                    }
                    break;
            }


            nextSymbol = getNextSymbol();
        }

        return nextToken;
    }


    /**
     * Check if the nextToken has just been initialized
     * @return True if we have a fresh / unused nextToken
     */
    private boolean isNewToken() {
        return nextToken.Type == null;
    }


    /**
     * Initialize the nextToken value. Null for the type
     * empty StringBuilder  for the value
     */
    private void initializeNextToken() {
        nextToken.Type = null;
        nextToken.Value = new StringBuilder();
        nextToken.IsComplete = true;
    }


    /**
     * Ignore the next value in the stream, move to the next one
     */
    protected void skipSymbol(){
        skipSymbol(1);
    }


    /**
     * Skip the next n symbols in the stream, move the cursor post them
     * @param positions The number of symbols to skip
     */
    protected void skipSymbol(int positions){
        position = position + positions;
    }


    /**
     * Consume the currently selected symbol, move the cursor to the next one
     */
    protected void consumeSymbol() {
        ((StringBuilder) nextToken.Value).append((char) inputBuffer[position]);
        position++;
    }


    /**
     * Part of the Iterator interface, no used in the current implementation
     */
    @Override
    public void remove() {
        // nothing really to do here
    }


    /**
     * Return the next symbol in the stream
     * @return Returns the next byte in the stream or -1 if the we try to read past the current buffer
     */
    private byte getNextSymbol() {
        return lookAhead(0);
    }


    /**
     * Return the n'th symbol following in the stream.
     * @param symbolsAhead How many position ahead should the symbol bee looked up
     * @return Returns the next byte in the stream or -1 if the we try to read past the current buffer
     */
    private byte lookAhead(int symbolsAhead) {
        if (inputBuffer.length <= position + symbolsAhead) {
            return -1;
        }

        return inputBuffer[position + symbolsAhead];
    }
}



