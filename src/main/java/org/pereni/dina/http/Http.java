package org.pereni.dina.http;


/**
 * General class for grouping HTTP related constants
 * @author Remus Pereni <remus@pereni.org>
 */
public final class Http {

    /**
     * Http end of line
     */
    public static final String EOL = "\r\n";

    /**
     * Http end of headers section
     */
    public static final String EOH = "\r\n";

    /**
     * Http white space
     */
    public static final char WS = ' ';

    /**
     * Http header separator
     */
    public static final char HEADER_SEPARATOR = ':';

    /**
     * Enumeration containing the possible HTTP methods
     */
    public static enum Method {
          CONNECT
        , DELETE
        , GET
        , HEAD
        , OPTIONS
        , POST
        , PUT
        , TRACE
    }

    /**
     * Encode the known HTTP versions
     */
    public static enum Version {
          HTTP_0_9
        , HTTP_1_0
        , HTTP_1_1
    }


    /**
     * A non exhaustive list of commonly used headers
     */
    public static class Headers {
        public static final String Cache_Control = "Cache-Control";
        public static final String Connection = "Connection";
        public static final String Server = "Server";
        public static final String Content_Length = "Content-Length";
        public static final String Content_Type = "Content-Type";
        public static final String Last_Modified = "Last-Modified";
        public static final String Accept = "Accept";
        public static final String Keep_Alive = "Keep-Alive";
        public static final String Host = "Host";
    }


    /**
     * Set of constants related to the Keep Alive and Connection headers
     */
    public static class Connection {
        public static final String Closed = "closed";
        public static final String Keep_Alive = "Keep-Alive";
        public static final String Max = "max";
        public static final String Time_Out = "timeout";
    }


    /**
     * UrlEncoded content type
     */
    public static class ContentType {
        public static final String UrlEncoded = "application/x-www-form-urlencoded";
    }
}
