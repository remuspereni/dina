package org.pereni.dina.http;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;


/**
 * Provides a default implementation of a ServletContext. The default implementation is minimalistic, the mime type
 * lookup implementation is based on a simple Java property files mime.properties that will be looked up in the
 * root of the class path while the servlet instance mapping is done with regular expressions.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class ServletContextImpl implements  ServletContext {

    /**
     * The default logger.
     */
    private static final Logger LOG = Logger.getLogger( ServletContextImpl.class.getName());


    /**
     * The name of the properties file used for storing the mime type associations
     */
    private static final String MIME_FILE = "/mime.properties";


    /**
     * The default mime type returned in case there is no match in the mime properties fields
     */
    private static final String APPLICATION_OCTET = "application/octet-stream";


    /**
     * The local store for the mime type associations.
     * The format used in the property file is: gif=image/gif
     */
    private static final Properties mimeTypes = new Properties();


    /**
     * A map with the Regexp pattern and the matched servlet instance used to store the url / servlet mappings.
     */
    private Map<Pattern, Class<? extends Servlet>> servletMountMap
                    = new LinkedHashMap<Pattern, Class<? extends Servlet>>();

    /**
     * The root path of the servlet context. Where on the file system this servlet context is mapped.
     */
    private String rootPath;


    /**
     * Default constructor for a servlet context implementation.
     *
     * @param rootPath the root path of the servlet context. Where on the file system this servlet context is mapped.
     */
    public ServletContextImpl(String rootPath) {

        File rootDir = new File( rootPath);
        try {

            if( !rootDir.exists() || !rootDir.isDirectory() ) {
                LOG.severe("The folder " + rootDir.getPath() + " does not exists or it is not a folder, exiting.");
                throw new IllegalStateException("Unable to find 'www' sub folder in "
                        + (new File("./")).getAbsolutePath()
                        + " to start the context");
            }

            this.rootPath =    rootDir.getCanonicalPath();
            LOG.info("Server root folder of context is: " +  this.rootPath);

        } catch (IOException e) {
            LOG.log( Level.SEVERE, "Server context path incorrect", e);
            throw new IllegalArgumentException("Servlet context path incorrect");
        }

        try {
            LOG.fine("Found mime.properties file at: " + getClass().getResource( MIME_FILE));
            mimeTypes.load( getClass().getResourceAsStream(MIME_FILE) );
        } catch (IOException e) {
            LOG.log( Level.SEVERE, "Unable to load mime types file", e);
            throw new IllegalArgumentException("Unable to load mime types file");
        }

        LOG.fine("Loaded " + mimeTypes.size() + " mime types");
    }


    /**
     * Gets the real path corresponding to the given request uri path.
     *
     * @param path the virtual path to be translated to a real path
     * @return the real path, or null if the translation cannot be performed
     */
    @Override
    public String getRealPath(String path) {
        if( !path.startsWith("/") )  path = "/" + path;
        return rootPath + path;
    }


    /**
     * Returns the MIME type of the specified file, or null if the MIME type is not known. The mime type
     * lookup implementation is based on a simple Java property files <code>mime.properties</code> that will be
     * looked up in the root of the class path.
     *
     * <p>The <code>mime.properties</code> will have the file extensions mapped on the mime types as can be seen
     * in the example bellow:</p>
     *
     * <pre>
     *     css=text/css
     *     gif=image/gif
     *     html=text/html
     *     ico=image/x-icon
     * </pre>
     *
     * @param file the name of a file
     * @return the file's MIME type
     */
    @Override
    public String getMimeType(String file) {
        int sepPos = file.lastIndexOf('.');
        if( sepPos == -1 ) return APPLICATION_OCTET;

        String extension = file.substring( file.lastIndexOf('.') + 1);
        if( mimeTypes.containsKey(extension) ) return (String) mimeTypes.get(extension);

        return APPLICATION_OCTET;
    }


    /**
     * Create a new <code>Servlet</code> instance that will handle the response of a request with the URI given as
     * parameter.
     *
     * @param requestUri the uri that will be used to determine the servlet handling the response
     * @return the servlet instance that will handle the request or null of there is no servlet instance mapped on the
     * request uri.
     */
    @Override
    public synchronized  Servlet instantiateServlet(String requestUri) {
        LOG.finer("Looking for the servlet mounted at " + requestUri + " between " + servletMountMap.size() + " mounts");

        for( Pattern urlPattern : servletMountMap.keySet() ) {

            if( urlPattern.matcher(requestUri).matches() ) {

                try {
                    LOG.finer("Found match, the request url " + requestUri + " maps to " + servletMountMap.get(urlPattern));

                    Class<? extends  Servlet> servletClass = servletMountMap.get(urlPattern);
                    return servletClass.newInstance();

                } catch (ExceptionInInitializerError e) {
                    e.printStackTrace();
                    LOG.log(Level.SEVERE, "Unable to instantiate servlet", e);
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    LOG.log(Level.SEVERE, "Unable to instantiate servlet", e);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    LOG.log(Level.SEVERE, "Unable to instantiate servlet", e);
                }
            }
        }

        LOG.log(Level.WARNING, "No servlet matching " + requestUri + " was found mounted");
        return null;
    }


    /**
     * Mount / set a servlet class to be instantiated on a specified request path regular expression. In this
     * implementation the order of insertion is important. A earlier mounted rule matching will have priority over
     * a potentially matching rule mounted later.
     * @param regexpPattern the regular expression that will be checked for matching on the request path
     * @param servletClass the servlet class name that will be instantiated in case the regular expression matches the
     *                     request url.
     */
    public void mount(String regexpPattern, Class<? extends Servlet> servletClass){
        servletMountMap.put(Pattern.compile(regexpPattern), servletClass);
    }

}
