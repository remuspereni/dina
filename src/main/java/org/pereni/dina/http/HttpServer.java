package org.pereni.dina.http;

import org.pereni.dina.server.ServerAdapter;
import org.pereni.dina.http.servlet.FileServlet;
import org.pereni.dina.http.servlet.SnoopServlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Provides an instance of an HTTP server. Built on top of the <code>Server</code> interface it initializes an
 * <code>ServletContext</code> in the <code>www</code> sub-folder of the current folder (the one in which the server
 * instance was started) and maps two <code>Servlet</code> instances (<code>SnoopServlet</code> to /snoop/* and
 * <code>FileServlet</code> to everything else) on the servlet context.
 *
 * It starts a <code>HttpDispatcher</code> instance on the above defined servlet context.
 *
 * @author Remus Pereni <remus@pereni.org>
 * @see org.pereni.dina.server.Server
 * @see org.pereni.dina.http.ServletContext
 * @see org.pereni.dina.http.servlet.SnoopServlet
 * @see org.pereni.dina.http.servlet.FileServlet
 * @see org.pereni.dina.http.HttpDispatcher
 */
public class HttpServer extends ServerAdapter {

    /**
     * Class logger.
     */
    private static Logger LOG = Logger.getLogger("");


    /**
     * Default constructor.
     * @throws IOException if the server is unable to open a socket channel on the requested port
     */
    public HttpServer() throws IOException {
        super();

        ServletContextImpl rootContext = new ServletContextImpl("./www");
        rootContext.mount("/snoop/.*", SnoopServlet.class);
        rootContext.mount(".*", FileServlet.class);
        setDispatcher(new HttpDispatcher(rootContext));
    }


    /**
     * Main method to actually create an instance of an HttpServer and start it.
     * @param args No arguments are accepted at this time. In the future the context path and server port would
     *             be welcomed.
     */
    public static void main(String[] args) {
        HttpServer server = null;

        try {
            server = new HttpServer();
            server.start();

            if( server.isRunning() ) {
                LOG.log(Level.INFO, "Server started on port {0}", server.getPort());
            }

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Unable to start server, reason {0}", e.getMessage());
        }
    }
}
