package org.pereni.dina.http.servlet;

import org.pereni.dina.http.Http;
import org.pereni.dina.http.HttpRequest;
import org.pereni.dina.http.HttpResponse;
import org.pereni.dina.http.Servlet;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Simple servlet to serve static files or directory listings
 * @author Remus Pereni <remus@pereni.org>
 */
public class FileServlet implements Servlet{

    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger( FileServlet.class.getName());


    /**
     * Initial part of the HTML code for directory listing
     */
    private static final String DIR_START_HTML = "<!DOCTYPE html>\n" +
            "<html lang=\"en-us\">\n" +
            "    <head>\n" +
            "        <meta charset=\"utf-8\"/>\n" +
            "        <title>Directory listing</title>  " +
            "        <style type=\"text/css\"> "+
            "           body { font-family: sans-serif; color:#444; margin: 2em; background-color:#fff; font-size: 90%%;} " +
            "           h1 { color:#5D8AA8; border-bottom:1px solid #ddd;} " +
            "           ul { color:#536895; list-style-type: square; } " +
            "           .dir { color:#536895; font-weight:bold; } " +
            "           .file { color:#536895; font-style:italic; } " +
            "        </style><body><ul>";

    /**
     * Message format to display each directory entry
     */
    private static final String DIR_ENTRY_HTML = "<li class=\"%s\"><a href=\"%s\">%s</a></li>";


    /**
     * Message format to display each directory entry in JSON format
     */
    private static final String DIR_ENTRY_JSON = "\"href\":\"%s\", \"name\":\"%s\", \"type\":\"%s\", \"size\":\"%s\"";


    /**
     * End part of the HTML content for the directory listing
     */
    private static final String DIR_END_HTML = "</ul></body/html>\n";


    /**
     * Error message in case the requested resource could not be found
     */
    private static final String URL_NOT_FOUND_HTML = "<!DOCTYPE html>\n" +
            "<html lang=\"en-us\">\n" +
            "    <head>\n" +
            "        <meta charset=\"utf-8\"/>\n" +
            "        <title>File not found</title>  " +
            "        <style type=\"text/css\"> "+
            "           body { font-family: sans-serif; color:#444; margin: 2em; background-color:#fff; font-size: 90%% ;} " +
            "           h1 { color:#5D8AA8; border-bottom:1px solid #ddd;} " +
            "           ul { color:#536895; list-style-type: square; } " +
            "        </style><body><h1>Not Found</h1><p>The requested URL %s was not found on this server</p><hr/></body></html>";

    /**
     * Constant parameter name used in the AJAX calls as the argument of the a move destination command
     */
    public static final String MOVETO = "moveto";


    /**
     * The main service method
     * @param request The request
     * @param response The response
     */
    @Override
    public void service(HttpRequest request, HttpResponse response) {
        try {

            // check if the requested uri maps to a file and if the file is actually readable
            // if the file does not exists or is not readable return 404 File Not Found
            File file = new File( request.getServletContext().getRealPath( request.getRequestURI()));
            if( !file.exists ()|| !file.canRead() ) {
                response.sendError( HttpResponse.SC_NOT_FOUND, "File not found");
                response.getWriter().print( String.format(URL_NOT_FOUND_HTML, request.getRequestURI()));
                response.getWriter().flush();
                return;
            }


            // we might have a move request from the Drag & Drop sample javascript application
            if( isRESTMoveRequest(request) ) {
                handleMoveRequest(file, request, response);
                return;
            }

            response.setStatus(HttpResponse.SC_OK);


            // if the file is a directory verify if the request is for the HTML version or for
            // the JSON version send the appropriate version
            if( file.isDirectory() ) {
                String rootPath = request.getRequestURI();
                if( rootPath.endsWith("/") ) rootPath = rootPath.substring(0, rootPath.length() -1);

                String acceptHeader = request.getHeader( Http.Headers.Accept);
                if(  acceptHeader != null && acceptHeader.trim().startsWith("application/json")) {
                    listFolderAsJson(file, rootPath, response);
                } else {
                    listFolder(file, rootPath, response);
                }

            } else {
                sendFile(file, request, response);
            }

            response.getWriter().flush();

        } catch ( IOException ex) {
            if( !response.isCommitted() ) {
                response.setStatus( HttpResponse.SC_INTERNAL_SERVER_ERROR);
            }
            LOG.log( Level.SEVERE, "Unable to send response", ex);
        }
    }


    /**
     * Determine if a request is an Ajax request to move a resource from one folder to another.
     *
     * @param request the request object
     * @return true if the request is an Ajax call to move a resource from one folder to another, false otherwise
     */
    private boolean isRESTMoveRequest(HttpRequest request) {
        return Http.Method.POST.name().equalsIgnoreCase( request.getMethod())
                && request.getParameter(MOVETO) != null;
    }


    /**
     * Handle the response to an Ajax resource move request. For now, we don't actually do any copy-ing, move-ing of
     * the resources because of security reasons but the operation itself could easily be added. Also because
     * some situations where wanted in which an error message is sent back as opposed to a success operation
     * we only allow files to be moved and only into different folders than are currently situated in.
     *
     * @param file the file handle of the requested file. If the file is not found then the servlet returns earlier
     *             an 404 File not found response.
     * @param request the request object
     * @param response the response object
     * @throws IOException in case some file operations fail
     */
    private void handleMoveRequest(File file, HttpRequest request, HttpResponse response) throws IOException {
        String destinationLocation = request.getParameter(MOVETO);
        File destinationFile = new File( request.getServletContext().getRealPath( destinationLocation));

        if( !destinationFile.exists() || !destinationFile.isDirectory() ) {

            response.sendError(HttpResponse.SC_BAD_REQUEST,
                    file.getName() + " can not be moved to " + destinationLocation
                            + " since the destination location does not exist or it is not a directory.");
            response.flushBuffer();
            return;
        }

        if( file.isDirectory() ) {
            response.sendError(HttpResponse.SC_BAD_REQUEST,
                    file.getName() + " is a directory, we only allow files to be moved to "
                            + destinationLocation);
            response.flushBuffer();
            return;
        }

        if( file.getPath().startsWith( destinationFile.getPath().substring(0, destinationFile.getPath().length() - 1))){
            response.sendError(HttpResponse.SC_BAD_REQUEST,
                    file.getName() + " is already in the destination location "
                            + destinationLocation + " ignoring request!");
            response.flushBuffer();
            return;
        }

        response.setStatus(HttpResponse.SC_OK);
        response.setContentType("text/plain");
        response.getWriter().println( file.getName() + " was successfully moved to " + destinationLocation
                + " ( not really, just kidding :) )");
        response.getWriter().flush();
        response.flushBuffer();
    }


    /**
     * Send a HTML representation of the folder's list
     * @param file
     * @param response
     */
    private void listFolder(File file, String basePath, HttpResponse response) throws IOException {
        response.setContentType( "text/html");

        PrintWriter writer = response.getWriter();
        writer.print( DIR_START_HTML);

        writer.println("<h1>Directory listing for " + basePath + "/</h1>");

        for( File fileItem : file.listFiles() ) {
            writer.print( String.format( DIR_ENTRY_HTML
                    , fileItem.isDirectory() ? "dir" : "file"
                    , basePath + "/" + fileItem.getName()
                    , fileItem.getName()));
        }

        writer.print( DIR_END_HTML);
    }


    /**
     * Send a JSON representation of the folder list
     * @param file
     * @param basePath
     * @param response
     * @throws IOException
     */
    private void listFolderAsJson(File file, String basePath, HttpResponse response) throws IOException {

        response.setContentType( "application/json");

        PrintWriter writer = response.getWriter();
        writer.print("{ \"name\" : \"" + file.getName() + "\"");

        String path="./";
        if( !response.getServletContext().getRealPath("/").equals( file.getPath() + "/")) {
            path += (file.getPath() + "/").substring( response.getServletContext().getRealPath("/").length() );
        }
        writer.print(" ,\"path\" : \"" +  path );
        writer.print("\" ,\"files\" : [");

        File[] files  = file.listFiles();
        for( int idx = 0; idx < files.length; idx++ ) {

            writer.print('{');
            writer.print( String.format( DIR_ENTRY_JSON
                    , basePath + "/" + files[idx].getName()
                    , files[idx].getName()
                    , files[idx].isDirectory() ? "directory" : "file"
                    , files[idx].length()
                ));
            writer.print("}\n");
            if( idx < files.length -1 ) writer.print(',');
        }
        writer.print( "]}");
    }


    /**
     * The actual request was for a file, make sure to send the content of the file.
     * @param file
     */
    private void sendFile( File file, HttpRequest request, HttpResponse response) throws IOException {

        response.setContentType( request.getServletContext().getMimeType( file.getName()));
        response.setContentLength( file.length() );
        response.setDateHeader( Http.Headers.Last_Modified, file.lastModified());

        response.flushBuffer();

        if( !"head".equalsIgnoreCase(request.getMethod())) {
            RandomAccessFile fileHandle = new RandomAccessFile(file, "r");
            fileHandle.getChannel().transferTo(0, fileHandle.length(), response.getOutputChannel());
            fileHandle.close();
            response.flushBuffer();
        }
    }


    /**
     * Not much to do here, no resources to free.
     */
    @Override
    public void destroy() {

    }
}
