package org.pereni.dina.http.servlet;


import org.pereni.dina.http.HttpRequest;
import org.pereni.dina.http.HttpResponse;
import org.pereni.dina.http.Servlet;

import java.io.IOException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet that displays the details of the request it receives.
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class SnoopServlet implements Servlet {

   /**
    * Logger
    */
    private static final Logger LOG = Logger.getLogger( SnoopServlet.class.getName());


    /**
     * Initial part of the HTML code for directory listing
     */
    private static final String START_HTML = "<!DOCTYPE html>\n" +
            "<html lang=\"en-us\">\n" +
            "    <head>\n" +
            "        <meta charset=\"utf-8\"/>\n" +
            "        <title>Snoop servlet</title>  " +
            "        <style type=\"text/css\"> "+
            "           body { font-family: sans-serif; color:#444; margin: 2em; background-color:#fff; font-size: 90%;} " +
            "           h1 { color:#5D8AA8; border-bottom:1px solid #ddd;} " +
            "           dt { color:#536895; font-style:italic;} " +
            "        </style><body>";

    /**
     * Message format to display each directory entry
     */
    private static final String ENTRY_HTML = "<dt>%s</dt><dd>%s</dd>\n";


    /**
     * End part of the HTML content for the direcotry listing
     */
    private static final String END_HTML = "</body/html>\n";


    @Override
    public void service(HttpRequest request, HttpResponse response) {

        try {

            response.setStatus(HttpResponse.SC_OK);
            response.getWriter().println(START_HTML);

            response.getWriter().println("<h1>Request Generics</h1>");
            response.getWriter().println("<dl>");
            response.getWriter().println(  String.format(ENTRY_HTML, "Method", request.getMethod()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Request Uri", request.getRequestURI()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Path Info", request.getPathInfo()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Path Translated", request.getPathTranslated()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Content Type", request.getContentType()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Content Length", request.getContentLength()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Character Encoding", request.getCharacterEncoding()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Query String", request.getQueryString()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Remote Address", request.getRemoteAddr()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Remote Port", request.getRemotePort()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Remote Host", request.getRemoteHost()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Local Address", request.getLocalAddr()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Local Port", request.getLocalPort()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Local Host", request.getLocalName()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Server Name", request.getServerName()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Server Port", request.getServerPort()));
            response.getWriter().println(  String.format(ENTRY_HTML, "Request URL", request.getRequestURL()));

            response.getWriter().println("</dl>");

            response.getWriter().println("<h1>Headers</h1>");
            response.getWriter().println("<dl>");

            // we'll just ignore potential multiple values for now
            Enumeration<String> headerNames = request.getHeaderNames();
            while( headerNames.hasMoreElements() ) {
                String headerName = headerNames.nextElement();
                response.getWriter().println(  String.format(ENTRY_HTML, headerName, request.getHeader(headerName)));
            }

            response.getWriter().println("</dl>");

            response.getWriter().println("<h1>Parameters</h1>");
            response.getWriter().println("<dl>");

            // we'll just ignore potential multiple values for now
            Enumeration<String> parameterNames = request.getParameterNames();
            while( parameterNames.hasMoreElements() ) {
                String paramName = parameterNames.nextElement();
                response.getWriter().println(  String.format(ENTRY_HTML, paramName, request.getParameter(paramName)));
            }

            response.getWriter().println("</dl>");
            response.getWriter().println(END_HTML);
            response.getWriter().flush();

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Exception during response", e);
        }
    }

    @Override
    public void destroy() {

    }
}
