package org.pereni.dina;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.pereni.dina.http.*;
import org.pereni.dina.util.MultiMapTest;


/**
 * Teste suite containing all the tests cases.
 * @author Remus Pereni <remus@pereni.org>
 */
@RunWith( Suite.class)
@Suite.SuiteClasses( {
          HttpLexerTest.class
        , HttpRequestHandlerTest.class
        , MultiMapTest.class
        , HttpRequestImplTest.class
        , HttpResponseImplTest.class
        , ServletContextImplTest.class
    })

public class AllTests {

}
