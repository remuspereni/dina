package org.pereni.dina.http;

import junit.framework.Assert;
import org.junit.Test;
import org.pereni.dina.server.ServerRuntimeException;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import static org.mockito.Mockito.mock;

/**
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class HttpRequestImplTest {

    private ServletContext mockContext = mock( ServletContext.class );
    private HttpDispatcher mockDispatcher = mock( HttpDispatcher.class );


    @Test(expected = ServerRuntimeException.class)
    public void testSetMethodIncorrect() throws Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setMethod(null);
    }

    @Test
    public void testSetMethod() throws Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        Assert.assertEquals( Http.Method.GET.name(), request.getMethod());
        request.setMethod( Http.Method.PUT );
        Assert.assertEquals( Http.Method.PUT.name(), request.getMethod());
    }

    @Test(expected = ServerRuntimeException.class)
    public void testSetVersionNull() throws Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setMethod(null);
    }

    @Test(expected = ServerRuntimeException.class)
    public void testSetVersionNotSupported() throws Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setVersion(Http.Version.HTTP_0_9);
    }

    @Test
    public void testSetVersionOK() throws Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setVersion( Http.Version.HTTP_1_1 );
        Assert.assertEquals( Http.Version.HTTP_1_1, request.getVersion() );
    }

    @Test(expected = ServerRuntimeException.class)
    public void testSetRequestURIIncorrect() throws  Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setRequestURI( new URI("test"));
    }

    @Test
    public void testSetRequestURIOK() throws  Exception {
        HttpRequestImpl request = new HttpRequestImpl();
        request.setRequestURI( new URI("/"));
    }


    @Test
    public void testContentTypeSimple() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.1\r\n"
                + "Content-Type: text/html\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("text/html", request.getContentType());
    }

    @Test
    public void testContentTypeComplete() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.1\r\n"
                + "Content-Type: text/html; charset=ISO-8859-1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("text/html", request.getContentType());
        Assert.assertEquals("iso-8859-1", request.getCharacterEncoding());
    }

    @Test
    public void testGetRequestParameters() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?name=value HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("value", request.getParameter("name"));
    }

    @Test
    public void testGetRequestParametersNoValue() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?name= HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("", request.getParameter("name"));
    }

    @Test
    public void testGetRequestParametersNoValueNoEqual() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?name HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("", request.getParameter("name"));
    }


    @Test
    public void testGetRequestParametersEncoding() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?name=va%20lue HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("va lue", request.getParameter("name"));
    }


    @Test
    public void testGetRequestParametersMultipleNames() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?param1=value1&param2=value2 HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("value1", request.getParameter("param1"));
        Assert.assertEquals("value2", request.getParameter("param2"));
    }


    @Test
    public void testGetRequestParametersMultipleValue() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET /?param1=value1&param2=value2&param1=value3 HTTP/1.1\r\n"
                + "\r\n").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("value1", request.getParameter("param1"));
        Assert.assertEquals("value2", request.getParameter("param2"));
        String[] results =     request.getParameterValues("param1");
        Assert.assertEquals(2, results.length);
        Assert.assertEquals("value1", results[0]);
        Assert.assertEquals("value3", results[1]);
    }


    @Test
    public void testPOSTRequestParametersMultipleValue() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("POST / HTTP/1.1\r\n"
                + "Content-Type: application/x-www-form-urlencoded\r\n\r\n"
                + "param1=value1&param2=value2&param1=value3").getBytes("US-ASCII")));

        HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);
        handler.setInputAvailable(inputChannel);
        handler.run();

        HttpRequest request = handler.getRequest();
        Assert.assertNotNull(request);
        Assert.assertEquals("value1", request.getParameter("param1"));
        Assert.assertEquals("value2", request.getParameter("param2"));
        String[] results =     request.getParameterValues("param1");
        Assert.assertEquals(2, results.length);
        Assert.assertEquals("value1", results[0]);
        Assert.assertEquals("value3", results[1]);
    }

}
