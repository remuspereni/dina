package org.pereni.dina.http;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.channels.Channels;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class HttpResponseImplTest {

    private static final Logger LOG = Logger.getLogger(HttpResponseImplTest.class.getName());

    ByteArrayOutputStream outputStream;
    HttpResponseImpl response;


    @Before
    public void setUp() {
        outputStream = new ByteArrayOutputStream();
        response = new HttpResponseImpl(Channels.newChannel(outputStream));
    }


    @Test
    public void testSendErrorCode() throws Exception {

        response.sendError( HttpResponse.SC_NOT_FOUND );
        response.finalizeResponse();
        String response = new String( outputStream.toByteArray(), "UTF-8");
        Assert.assertNotNull( response );
        Assert.assertTrue( response.startsWith("HTTP/1.1 404\r\n" ));
    }

    @Test
    public void testSendError() throws Exception {

        response.sendError( HttpResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error" );
        response.finalizeResponse();
        String response = new String( outputStream.toByteArray(), "UTF-8");
        Assert.assertNotNull( response );
        Assert.assertTrue( response.startsWith("HTTP/1.1 500 Internal Server Error\r\n"));
    }

    @Test
    public void testSendErrorTempBuffer() throws Exception {

        outputStream = new ByteArrayOutputStream();
        response = new HttpResponseImpl();
        response.sendError( HttpResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error" );
        response.getWriter().print("<html><body><h1>Error</h1><p>Internal Server Error</p></body></html>");
        response.setOutputChannel(Channels.newChannel(outputStream));
        response.finalizeResponse();
        String response = new String( outputStream.toByteArray(), "UTF-8");
        Assert.assertNotNull( response );
        Assert.assertTrue( response.startsWith("HTTP/1.1 500 Internal Server Error\r\n"));
    }


    @Test
    public void testSetStatusSimple() throws Exception {
        response.flushBuffer();
        response.finalizeResponse();
        String response = new String( outputStream.toByteArray(), "UTF-8");
        Assert.assertNotNull( response );
        Assert.assertTrue( response.startsWith("HTTP/1.1 200 OK\r\n"));
    }

}
