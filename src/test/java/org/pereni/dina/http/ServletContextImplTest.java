package org.pereni.dina.http;

import junit.framework.Assert;
import org.junit.Test;
import org.pereni.dina.http.servlet.FileServlet;

import java.util.logging.Logger;

/**
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class ServletContextImplTest {

    private static final Logger LOG = Logger.getLogger( ServletContextImplTest.class.getName());

    private ServletContextImpl servletContext = new ServletContextImpl("./");



    @Test
    public void testGetRealPath() throws Exception {
        Assert.assertTrue( servletContext.getRealPath("index.html").endsWith("/index.html"));
    }

    @Test
    public void testGetMimeType() throws Exception {
        Assert.assertEquals("text/html", servletContext.getMimeType("index.html"));
    }


    @Test
    public void testInstanciateNewServlet() throws Exception {
        servletContext.mount(".*", FileServlet.class);
        Assert.assertNotNull( servletContext.instantiateServlet("/") );
    }

}
