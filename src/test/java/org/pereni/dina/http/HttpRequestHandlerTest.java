package org.pereni.dina.http;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

import static org.mockito.Mockito.mock;

public class HttpRequestHandlerTest {

    private ServletContext mockContext = mock( ServletContext.class );
    private HttpDispatcher mockDispatcher = mock( HttpDispatcher.class );
    private HttpRequestHandler handler = new HttpRequestHandler(mockDispatcher, null);



    @Before
    public void setUp() {
        mockDispatcher.setServletContext( mockContext );
    }

    @Test
    public void testInitialValuesOfStateForRequestHandler() throws Exception {
        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.Start, handler.getCurrentStateHandler().getHandledState());
    }

    @Test
    public void testRequestVersion10() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index.html HTTP/1.0\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertEquals(Http.Version.HTTP_1_0, handler.getRequest().getVersion());
    }

    @Test
    public void testRequestVersion11() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index.html HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertEquals(Http.Version.HTTP_1_1, handler.getRequest().getVersion());
    }

    @Test
    public void testRequestMethodGET() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index.html HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertEquals(Http.Method.GET.name(), handler.getRequest().getMethod());
    }


    @Test
    public void testRequestMethodPOST() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("POST /index.html HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertEquals(Http.Method.POST.name(), handler.getRequest().getMethod());
    }


    @Test
    public void testStateForRequestHandlerStarting() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index.html HTTP/1.1".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.Start, handler.getCurrentStateHandler().getHandledState());
    }


    @Test
    public void testStateForRequestHandlerStartingPartialProcessing() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.Start, handler.getCurrentStateHandler().getHandledState());

        inputChannel = Channels.newChannel(new ByteArrayInputStream(".html HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.ProcessingHeaders, handler.getCurrentStateHandler().getHandledState());
        Assert.assertEquals(Http.Method.GET.name(), handler.getRequest().getMethod());
    }



    @Test
    public void testStateForRequestHandlerStartTransition() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("GET /index.html HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.ProcessingHeaders, handler.getCurrentStateHandler().getHandledState());
    }


    @Test
    public void testRequestInvalidRequestLine() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("\rPOST HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        WritableByteChannel outputChannel = Channels.newChannel( bos );

        handler.setOutputAvailable( outputChannel);
        handler.run();

        String response = new String(bos.toByteArray(), "UTF-8");
        Assert.assertTrue( response.startsWith("HTTP/1.1 400 Bad request"));

        outputChannel.close();
    }


    @Test
    public void testRequestInvalidURI() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream("POST testResource HTTP/1.1\r\n".getBytes("US-ASCII")));
        handler.setInputAvailable(inputChannel);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        WritableByteChannel outputChannel = Channels.newChannel( bos );

        handler.setOutputAvailable( outputChannel);
        handler.run();

        outputChannel.close();

        String response = new String(bos.toByteArray(), "UTF-8");
        Assert.assertTrue( response.startsWith("HTTP/1.1 400 Bad request"));
    }


    @Test
    public void testProcessHeaders() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.1\r\n"
                + "User-Agent: Java/1.7.0_17\r\n"
                + "Host: localhost:8081\r\n"
                + "Accept: text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2\r\n"
                + "\r\n").getBytes("US-ASCII")));

        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertNotNull( handler.getCurrentStateHandler() );
        Assert.assertEquals(HttpRequestHandler.RequestState.ReadyToRespond, handler.getCurrentStateHandler().getHandledState());

        Assert.assertEquals(3, handler.getRequest().getHeaders().size());
    }


    @Test
    public void testKeepAliveDefaultVersion1_0() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.0\r\n"
                + "\r\n").getBytes("US-ASCII")));

        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertFalse( handler.isKeepAlive() );
    }


    @Test
    public void testKeepAliveDefaultVersion1_1() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.1\r\n"
                + "User-Agent: Java/1.7.0_17\r\n"
                + "\r\n").getBytes("US-ASCII")));

        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertTrue( handler.isKeepAlive() );
    }


    @Test
    public void testKeepAliveVersion1_0() throws Exception {
        ReadableByteChannel inputChannel = Channels.newChannel(new ByteArrayInputStream(("GET / HTTP/1.0\r\n"
                + "Connection: Keep-Alive\r\n"
                + "Keep-Alive: timeout=500\r\n"
                + "\r\n").getBytes("US-ASCII")));

        handler.setInputAvailable(inputChannel);
        handler.run();

        Assert.assertTrue( handler.isKeepAlive() );
    }


}
