package org.pereni.dina.http;

import junit.framework.Assert;
import org.junit.Test;
import java.util.Iterator;


public class HttpLexerTest {

    @Test(expected = IllegalArgumentException.class)
    public void testIteratorOnNullBuffer() throws Exception {
        new HttpLexer(null);
    }

    @Test
    public void testIteratorTokenTypeEOL() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer(new byte[]{ HttpLexer.CARRIAGE_RETURN, HttpLexer.LINE_FEED});
        Assert.assertTrue(iterator.hasNext());

        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeEOF() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer(new byte[]{ HttpLexer.CARRIAGE_RETURN, HttpLexer.LINE_FEED, HttpLexer.CARRIAGE_RETURN, HttpLexer.LINE_FEED});
        Assert.assertTrue(iterator.hasNext());

        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOF ", HttpLexer.TokenType.EOF, token.Type);
    }


    @Test
    public void testIteratorTokenTypeLiteral() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP", token.Value.toString());
    }


    @Test
    public void testIteratorTokenTypeLiteralCombined() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HT".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HT ", "HT", token.Value.toString());

        iterator = new HttpLexer( "TP".getBytes("US-ASCII"), token);
        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP", token.Value.toString());
    }

    @Test
    public void testIteratorTokenTypeLiteralIsComplete() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HT".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertFalse( token.IsComplete );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HT ", "HT", token.Value.toString());

        iterator = new HttpLexer( "TP\r\n".getBytes("US-ASCII"), token);
        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertTrue( token.IsComplete );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP", token.Value.toString());
    }


    @Test
    public void testIteratorTokenTypeSeparator() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( ":".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Separator ", HttpLexer.TokenType.SEPARATOR, token.Type);
        Assert.assertEquals("TokenType value is : ", ":", token.Value.toString());
    }


    @Test
    public void testIteratorTokenTypeLiteralWithEOL() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeLiteralWithEOF() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOF ", HttpLexer.TokenType.EOF, token.Type);
    }


    @Test
    public void testIteratorTokenTypeLiteralWithFoldingSpace() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n HELLO\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP HELLO", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeLiteralWithFoldingTab() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n\tHELLO\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP HELLO", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeLiteralWithFoldingSpaceExtraSpace() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n     HELLO\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP     HELLO", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeLiteralWithFoldingTabExtraSpace() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "HTTP\r\n\t     HELLO\r\n".getBytes("US-ASCII"));
        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "HTTP      HELLO", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeSeparatorSingle() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "KEY:VALUE\r\n".getBytes("US-ASCII"));

        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "KEY", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "VALUE", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeSeparatorMultipleFirst() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "KEY::VALUE\r\n".getBytes("US-ASCII"));

        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "KEY", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", ":VALUE", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }


    @Test
    public void testIteratorTokenTypeSeparatorMultipleIn() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "KEY:VAL : UE\r\n".getBytes("US-ASCII"));

        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "KEY", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "VAL : UE", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }

    @Test
    public void testIteratorTokenTypeSeparatorMultipleInFolded() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( "KEY:VAL \r\n\t : UE\r\n".getBytes("US-ASCII"));

        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "KEY", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value is HTTP ", "VAL   : UE", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);
    }


    @Test
    public void testIteratorTokenTypeActualHeaders() throws Exception {
        Iterator<HttpLexer.Token> iterator = new HttpLexer( ("GET / HTTP/1.1\r\n"
                + "User-Agent: Java/1.7.0_17\r\n"
                + "Host: localhost:8081\r\n"
                + "Accept: text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2\r\n"
                + "\r\n").getBytes("US-ASCII"));


        Assert.assertTrue(iterator.hasNext());
        HttpLexer.Token token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value ", "GET / HTTP/1.1", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", "User-Agent", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", " Java/1.7.0_17", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", "Host", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", " localhost:8081", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOL ", HttpLexer.TokenType.EOL, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", "Accept", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is SEPARATOR ", HttpLexer.TokenType.SEPARATOR, token.Type);

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is Literal ", HttpLexer.TokenType.LITERAL, token.Type);
        Assert.assertEquals("TokenType value", " text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2", token.Value.toString());

        Assert.assertTrue(iterator.hasNext());
        token = iterator.next();
        Assert.assertNotNull( token );
        Assert.assertEquals("TokenType is EOF ", HttpLexer.TokenType.EOF, token.Type);
    }

}
