package org.pereni.dina.util;

import junit.framework.Assert;
import org.junit.Test;

import java.util.List;

/**
 *
 * @author Remus Pereni <remus@pereni.org>
 */
public class MultiMapTest {


    @Test
    public void testPutSingleGetSingle() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        Assert.assertEquals("1", map.getValue("test"));
    }


    @Test
    public void testPutSingleGetList() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        Assert.assertEquals(1, map.getValues("test").size());
        Assert.assertTrue( map.getValues("test") instanceof List);
        Assert.assertEquals("1", map.getValues("test").get(0));
    }


    @Test
    public void testPutDoubleGetSingle() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        map.put("test", "2");
        Assert.assertEquals("1", map.getValue("test"));
    }

    @Test
    public void testPutDoubleGetDouble() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        map.put("test", "2");
        Assert.assertEquals(2, map.getValues("test").size());
        Assert.assertTrue( map.getValues("test") instanceof List);
        Assert.assertEquals("1", map.getValues("test").get(0));
        Assert.assertEquals("2", map.getValues("test").get(1));
    }


    @Test
    public void testPutSingleGetArray() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        String[] results = map.getValuesAsArray("test");
        Assert.assertEquals(1, results.length);
        Assert.assertEquals("1", results[0]);
    }


    @Test
    public void testPutDoubleGetArray() throws Exception {
        MultiMap map = new MultiMap();
        map.put("test", "1");
        map.put("test", "2");
        String[] results = map.getValuesAsArray("test");
        Assert.assertEquals(2, results.length);
        Assert.assertEquals("1", results[0]);
        Assert.assertEquals("2", results[1]);
    }

}
